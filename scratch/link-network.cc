/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/linkapplication-helper.h"
#include"ns3/object.h"
#include <iostream>
#include<iostream>
#include<string>
#include<fstream>
#include<sstream>
#include "ns3/netanim-module.h"
#include "ns3/link-scenario.h"
#include"ns3/mobility-module.h"
// Default Network Topology
//
//       10.1.1.0
// n0 -------------- n1
//    point-to-point
//
 
using namespace ns3;


int
main (int argc, char *argv[])
{

  CommandLine cmd (__FILE__);
  cmd.Parse (argc, argv);
  
  Time::SetResolution (Time::NS);
  //LogComponentEnable ("LinkClient", LOG_LEVEL_INFO);
  //LogComponentEnable ("LinkServer", LOG_LEVEL_INFO);
  //LogComponentEnable ("DefaultGenerate", LOG_LEVEL_INFO);
  //LogComponentEnable ("DefaultOrchestrator", LOG_LEVEL_INFO);
  //LogComponentEnable ("LinkReceive", LOG_LEVEL_INFO);
  std::map<double,double> delay0;
  delay0[0.02]=100;
  delay0[0.03]=15;
  delay0[0.04]=7.5;
  delay0[1]=3.75;
  int little0 =0;
  std::map<double,double> delay1;
  delay1[0.04]=100;
  delay1[0.05]=15;
  delay1[1]=7.5;
  int little1 =0;
//  std::map<double,double> delay2;
//  delay2[0.03]=100;
//  delay2[0.04]=15;
//  delay2[0.06]=5;
//  delay2[1]=3.75;

  double prob0 = rand()/double(RAND_MAX);
  auto d0 = find_if(delay0.begin(),delay0.end(),[prob0](std::pair<double,double> delaypair){return prob0<delaypair.first;} );
  if(d0->second!=3.75)
  {
      little0++;
  }
  double prob1 = rand()/double(RAND_MAX);
  auto d1 = find_if(delay1.begin(),delay1.end(),[prob1](std::pair<double,double> delaypair){return prob1<delaypair.first;} );
  if(d1->second!=7.5)
  {
      little1++;
  }
  //double prob2 = rand()/double(RAND_MAX);
  //auto d2 = find_if(delay0.begin(),delay0.end(),[prob2](std::pair<double,double> delaypair){return prob2<delaypair.first;} );


  //double totalTime = 200;//500秒
  int interval = 40;
  double send = 200;//仿真100000次
  double totalTime = interval*send;
  std::string s="Delay";

  NodeContainer nodes;
  nodes.Create (4);

  PointToPointHelper pointToPoint0;
  pointToPoint0.SetDeviceAttribute ("DataRate", StringValue ("10Mbps"));
  pointToPoint0.SetChannelAttribute ("Delay", StringValue (std::to_string(d0->second)+"s"));
  PointToPointHelper pointToPoint1;
  pointToPoint1.SetDeviceAttribute ("DataRate", StringValue ("10Mbps"));

  pointToPoint1.SetChannelAttribute ("Delay", StringValue (std::to_string(d1->second)+"s"));
  PointToPointHelper pointToPoint2;
  pointToPoint2.SetDeviceAttribute ("DataRate", StringValue ("10Mbps"));
  pointToPoint2.SetChannelAttribute ("Delay", StringValue (std::to_string(0)+"s"));

    for(double time=0;time!=totalTime;time+=totalTime/send)
    {
        if(time ==0)
        {
            continue;
        }
  double prob0 = rand()/double(RAND_MAX);
  auto d00 = find_if(delay0.begin(),delay0.end(),[prob0](std::pair<double,double> delaypair){return prob0<delaypair.first;} );
  if(d00->second!=3.75)
  {
      little0++;
  }
  double prob1 = rand()/double(RAND_MAX);
  auto d11 = find_if(delay1.begin(),delay1.end(),[prob1](std::pair<double,double> delaypair){return prob1<delaypair.first;} );
  if(d11->second!=7.5)
  {
      little1++;
  }
  //double prob2 = rand()/double(RAND_MAX);
  //auto d2 = find_if(delay0.begin(),delay0.end(),[prob2](std::pair<double,double> delaypair){return prob2<delaypair.first;} );
        Simulator::Schedule(Seconds(time-0.000001),&PointToPointHelper::SetChannelAttribute,&pointToPoint0,s,StringValue (std::to_string(d00->second)+"s"));
        Simulator::Schedule(Seconds(time-0.000001),&PointToPointHelper::SetChannelAttribute,&pointToPoint1,s,StringValue (std::to_string(d11->second)+"s"));
       // Simulator::Schedule(Seconds(time-0.000001),&PointToPointHelper::SetChannelAttribute,&pointToPoint2,s,StringValue (std::to_string(d2->second)+"s"));
    }

  NetDeviceContainer devices;
  auto devices01 = pointToPoint0.Install (nodes.Get(0),nodes.Get(1));
  auto devices12 = pointToPoint1.Install (nodes.Get(1),nodes.Get(2));
  auto devices23 = pointToPoint2.Install (nodes.Get(2),nodes.Get(3));



  InternetStackHelper stack;
  stack.Install (nodes);

  Ipv4AddressHelper address01;
  address01.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4AddressHelper address12;
  address12.SetBase ("10.1.2.0", "255.255.255.0");
  Ipv4AddressHelper address23;
  address23.SetBase ("10.1.3.0", "255.255.255.0");

  Ipv4InterfaceContainer interfaces01 = address01.Assign (devices01);
  Ipv4InterfaceContainer interfaces12 = address01.Assign (devices12);
  Ipv4InterfaceContainer interfaces23 = address01.Assign (devices23);

  LinkClientHelper echoClient0 (interfaces01.GetAddress (1), 10);
  ApplicationContainer clientApps_0 = echoClient0.Install (nodes.Get (0));
  clientApps_0.Start (Seconds (0.0));
  clientApps_0.Stop (Seconds (totalTime));
  LinkClientHelper echoClient1 (interfaces23.GetAddress (0), 10);
  ApplicationContainer clientApps_1 = echoClient1.Install (nodes.Get (1));
 // ApplicationContainer clientApps_1 = echoClient.Install (nodes.Get (1));
  clientApps_1.Start (Seconds (0.0));
  clientApps_1.Stop (Seconds (totalTime));
  LinkClientHelper echoClient2 (interfaces23.GetAddress (1), 10);
  ApplicationContainer clientApps_2 = echoClient2.Install (nodes.Get (2));
  clientApps_2.Start (Seconds (0.0));
  clientApps_2.Stop (Seconds (totalTime));
  //LinkClientHelper echoClient3 (interfaces23.GetAddress (0), 10);
//  LinkClientHelper echoClient3 (Ipv4Address(100), 10);
//  ApplicationContainer clientApps_3 = echoClient3.Install (nodes.Get (3));
//  clientApps_3.Start (Seconds (0.0));
//  clientApps_3.Stop (Seconds (totalTime));

  LinkServerHelper echoServer0 (10);
  ApplicationContainer serverApps0 = echoServer0.Install (nodes.Get(0));
  serverApps0.Start (Seconds (0.0));
  serverApps0.Stop (Seconds (totalTime));
  LinkServerHelper echoServer1 (10);
  ApplicationContainer serverApps1 = echoServer1.Install (nodes.Get(1));
  serverApps1.Start (Seconds (0.0));
  serverApps1.Stop (Seconds (totalTime));
  LinkServerHelper echoServer2 (10);
  ApplicationContainer serverApps2 = echoServer2.Install (nodes.Get(2));
  serverApps2.Start (Seconds (0.0));
  serverApps2.Stop (Seconds (totalTime));

  LinkServerHelper echoServer3 (10);
  ApplicationContainer serverApps3 = echoServer3.Install (nodes.Get(3));
  serverApps3.Start (Seconds (0.0));
  serverApps3.Stop (Seconds (totalTime));

//  MobilityHelper mobility;
//  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
//          "MinX", DoubleValue (0.0),
//          "MinY", DoubleValue (15.0),
//          "DeltaX", DoubleValue (10),
//          "DeltaY", DoubleValue (),
//          "GridWidth", UintegerValue (5),
//          "LayoutType", StringValue ("RowFirst"));
//  mobility.Install (nodes); 
//  mobility.SetMobilityModel ("ns3::ConstantVelocityMobilityModel");

  srand((unsigned)time(NULL));

    /* 向指定node安装Manager*/
  //helper 内安装的仍是Manager而不是LinkManager，这个要改
  //下面的也要该
    LinkManagerHelper manager;
    manager.Install(nodes);
    auto manager_0 = nodes.Get(0)->GetObject<LinkManager>();
    auto manager_1 = nodes.Get(1)->GetObject<LinkManager>();
    auto manager_2 = nodes.Get(2)->GetObject<LinkManager>();
    auto manager_3 = nodes.Get(3)->GetObject<LinkManager>();
    manager_0->SetMachineId("1000");
    manager_1->SetMachineId("2000");
    manager_2->SetMachineId("3000");
    manager_3->SetMachineId("4000");
    /* 构建machineId与IP地址的映射关系*/
    Address machine_0 = interfaces01.GetAddress(0);
    Address machine_1 = interfaces01.GetAddress(1);
    Address machine_2 = interfaces12.GetAddress(1);
    Address machine_3 = interfaces23.GetAddress(1);
    std::map<std::string,Address> map;
    map.insert(std::make_pair("1000",machine_0));
    map.insert(std::make_pair("2000",machine_1));
    map.insert(std::make_pair("3000",machine_2));
    map.insert(std::make_pair("4000",machine_3));
    manager_0->SetMap(map);
    manager_1->SetMap(map);
    manager_2->SetMap(map);
    manager_3->SetMap(map);
    std::map<double,double> resource;
    //resource[0] = 1;
    resource[0.01] = 1;
    resource[0.04] = 2;
    resource[0.14] = 3;
    resource[0.39] = 4;
    resource[0.76] = 5;
    resource[1] = 6;

    manager_0->SetResourceDis(resource);
    manager_1->SetResourceDis(resource);
    manager_2->SetResourceDis(resource);
    manager_3->SetResourceDis(resource);

    LinkTask sample(16,0,{"1000","2000","3000","4000"});
//    for(double time=0;time!=totalTime;time+=totalTime/send)
//    {
//        Simulator::Schedule(Seconds(time-0.000001),&PointToPointHelper::SetChannelAttribute,&pointToPoint1,s,StringValue("20s"));
//
//    }
    manager_0->InitialScene(sample,totalTime,send);
    //Simulator::Schedule(Seconds(300),&LinkManager::PrintResult,manager_0);
    manager_0->Start();
    Simulator::Stop(Seconds(3*totalTime));
  Simulator::Run ();
//    Simulator::Schedule(Seconds(3.0),&Manager::GetTaskInfo,no_manager,{"s00001","2","2","3","aaa"});
  Simulator::Destroy ();
  std::cout<<little0<<'\t'<<little1<<std::endl;
  return 0;
}
