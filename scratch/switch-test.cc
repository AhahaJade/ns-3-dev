#include"ns3/undigraph-statistic.h"
using namespace ns3;
int main()
{
    UndigraphStatistic sta(261);
    sta.SetListFile("/home/ssf/google-cluster/my_data/switch_table.csv");
    sta.ReadListFile();
    for(int i=0;i!=26;++i)
    {
        std::string path = "/home/ssf/google-cluster/my_data/statistic/test_";
        std::string suffix = ".csv";
        std::string nowPath = path + std::to_string(i)+suffix;
        sta.SetFilePath(nowPath);
        sta.ReadFile();
    }
//    sta.SetFilePath("/home/ssf/google-cluster/my_data/statistic/test_0.csv");
//    sta.SetFilePath("/home/ssf/google-cluster/my_data/test.csv");
//    sta.ReadFile();
    sta.SaveData("/home/ssf/google-cluster/my_data/statistic-graph/graph-test.csv");
    return 0;
}
