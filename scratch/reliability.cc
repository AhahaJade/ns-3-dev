#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/netanim-module.h"
namespace ns3{

    namespace TopologyOne{
        void CreateTopology()
        {
            LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
            LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);

//			NodeContainer node01;
//			NodeContainer node12;
//			NodeContainer node23;
//
//            node01.Create(2);
//            node12.Create(2);
//            node23.Create(2);
            NodeContainer nodes;
            nodes.Create(4);
			
			PointToPointHelper link01;
            link01.SetDeviceAttribute("DataRate",StringValue("5Mbps"));
			link01.SetChannelAttribute ("Delay", StringValue("2ms"));

			PointToPointHelper link12;
            link12.SetDeviceAttribute("DataRate",StringValue("5Mbps"));
			link12.SetChannelAttribute ("Delay", StringValue("2ms"));

			PointToPointHelper link23;
            link23.SetDeviceAttribute("DataRate",StringValue("5Mbps"));
			link23.SetChannelAttribute ("Delay", StringValue("2ms"));
            
            NetDeviceContainer device01;
            NetDeviceContainer device12;
            NetDeviceContainer device23;

            device01=link01.Install(nodes.Get(0),nodes.Get(1));
            device12=link12.Install(nodes.Get(1),nodes.Get(2));
            device23=link23.Install(nodes.Get(2),nodes.Get(3));

            InternetStackHelper stack;
            stack.Install(nodes);
//            stack.Install(node01);
//            stack.Install(node12);
//            stack.Install(node23);

            Ipv4AddressHelper address01;
            address01.SetBase("10.1.1.0","255.255.255.0");

            Ipv4AddressHelper address12;
            address12.SetBase("10.1.2.0","255.255.255.0");

            Ipv4AddressHelper address23;
            address23.SetBase("10.1.3.0","255.255.255.0");

            Ipv4InterfaceContainer interface01 = address01.Assign(device01);
            Ipv4InterfaceContainer interface12 = address12.Assign(device12);
            Ipv4InterfaceContainer interface23 = address23.Assign(device23);

            UdpEchoServerHelper echoServer(8);
            //ApplicationContainer serverApps1 = echoServer.Install(node01.Get(1));
            ApplicationContainer serverApps1 = echoServer.Install(nodes.Get(1));
            serverApps1.Start(Seconds(0.0));
            serverApps1.Stop(Seconds(10.0));

            UdpEchoClientHelper echoClient0(interface01.GetAddress(0),8);
            echoClient0.SetAttribute("MaxPackets",UintegerValue(3));
            echoClient0.SetAttribute("Interval",TimeValue(Seconds(1)));
            echoClient0.SetAttribute("PacketSize",UintegerValue(1024));
//            auto client0 = echoClient0.Install(node01.Get(0));
            auto client0 = echoClient0.Install(nodes.Get(0));
            client0.Start(Seconds(2.0));
            client0.Stop(Seconds(10.0));
            
 //这里报错了           Ipv4GlobalRoutingHelper::PopulateRoutingTables();
            AnimationInterface anim("TopologyOne.xml");
        }
    }
}
using namespace ns3;
int main (int argc, char *argv[])
{
    //CommandLine cmd;
    //cmd.Parse(argc,argv);
    Time::SetResolution(Time::NS);
    TopologyOne::CreateTopology();
    Simulator::Run();
    Simulator::Destroy();
    return 0;
}
