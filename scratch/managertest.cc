/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * his program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include"ns3/core-module.h"
#include"ns3/resource-unit.h"
#include"ns3/resource-unit-container.h"
#include"ns3/resource-unit-helper.h"
#include"ns3/object.h"
#include <iostream>
#include"ns3/manager.h"
#include"ns3/manager-helper.h"
#include"ns3/stats-module.h"
#include<iostream>
#include<string>
#include<fstream>
#include<sstream>

using namespace ns3;
void
TraceCpu (double oldValue, double newValue)
{
  std::cout<<"At time "<<Simulator::Now().As(Time::S) << " the old value of cpu  " << oldValue << " has changed to " << newValue << std::endl;
}
void
TraceMem (double oldValue, double newValue)
{
  std::cout <<"At time "<<Simulator::Now().As(Time::S)<< " the old value of memory  " << oldValue << " has changed to " << newValue << std::endl;
}
void
StringTraceNowId (std::string oldValue, std::string newValue)
{
  std::cout << "At time _s,the old value of NowId " << oldValue << " has changed to " << newValue << std::endl;
#include"ns3/stats-module.h"
}
void
StringTraceNowStatus (std::string oldValue, std::string newValue)
{
  std::cout << "At time _s,the old status of task " << oldValue << " has changed to " << newValue << std::endl;
}
int
main (int argc, char *argv[])
{

    Time::SetResolution (Time::NS);
    NodeContainer nodes;
    nodes.Create(3);
    /* 向指定node安装资源部件*/
    ResourceHelper res;
    res.Install(nodes.Get(2));
    Ptr<Node> node = nodes.Get(2);
    Ptr<ResourceContainer> container= node->GetObject<ResourceContainer>();
//    Ptr<Resource> re2 = CreateObject<Resource>();
//    container->Add(re2);
    Ptr<Resource> MyResource = *(container->Begin());
//    re2->TraceConnectWithoutContext ("RemainingMemory", MakeCallback (&TraceMem));
//    re2->TraceConnectWithoutContext ("RemainingCpu", MakeCallback (&TraceCpu));
    MyResource->TraceConnectWithoutContext ("RemainingMemory", MakeCallback (&TraceMem));
    MyResource->TraceConnectWithoutContext ("RemainingCpu", MakeCallback (&TraceCpu));
    /* 向指定node安装Manager*/
    ManagerHelper<Manager> manager;
    manager.Install(nodes.Get(2));
    auto no_manager = nodes.Get(2)->GetObject<Manager>();
    MyResource->SetResourceId("2020001");
    MyResource->SetMemory(0.2493);
    MyResource->SetCpu(0.5);
    /* 读取数据信息*/
    std::ifstream in("/home/ssf/tarballs/EasiEI/ns-3-dev/scratch/final.csv");
    std::string line;
    getline(in,line);
    while(getline(in,line))
    {
        std::stringstream ss(line);
        std::string str;
        getline(ss,str,',');
        double time = std::stod(str)/10;
std::string        str_time =boost::lexical_cast<std::string>(time);
        getline(ss,str,',');
        std::string jobid =str;
        getline(ss,str,',');
        std::string job_stamp =str;
        std::string task_id = jobid+job_stamp;
        getline(ss,str,',');
        auto event =std::stod(str);
std::string        str_event =boost::lexical_cast<std::string>(event);
        getline(ss,str,',');
        auto prior =std::stod(str);
std::string        str_prior =boost::lexical_cast<std::string>(prior);
        getline(ss,str,',');
        double cpu =std::stod(str);
std::string        str_cpu =boost::lexical_cast<std::string>(cpu);
        getline(ss,str,',');
        double memory = std::stod(str);
std::string        str_memory =boost::lexical_cast<std::string>(memory);
        getline(ss,str,',');
        double end_time =std::stod(str)/10;
std::string        str_endTime =boost::lexical_cast<std::string>(end_time);
        no_manager->GetTaskInfo({task_id,str_cpu,str_memory,str_prior,"",str_time,str_endTime  });

    }
    no_manager->Start();

//    re2->SetResourceId("2020002");
//    re2->SetMemory(3);
//    re2->SetCpu(4);
//    no_manager->GetTaskInfo({"s00001","2","2","3","","3"});
//    no_manager->GetTaskInfo({"s00002","1","1","2","","3"});
//    no_manager->GetTaskInfo({"s00003","1","3","5","","5"});
//    no_manager->GetTaskInfo({"s00004","2","3","5","","6"});
   // no_manager->ShowTaskStatus();
//   no_manager->RunTask();
//    no_manager->GetTaskInfo();
//   no_manager->ShowTaskStatus();
//    no_manager->Start();
  Names::Add ("/Names/Emitter",MyResource );

  //
  // This file helper will be used to put data values into a file.
  //

  // Create the file helper.
  FileHelper fileHelper;

  // Configure the file to be written.
  fileHelper.ConfigureFile ("cpu2",
                            FileAggregator::FORMATTED);
  //fileHelper.ConfigureFile ("mem",
  //                          FileAggregator::FORMATTED);

  // Set the labels for this formatted output file.
//  fileHelper.Set2dFormat ("Time (Seconds) = %.3f\tCount = %lf");
  fileHelper.Set2dFormat ("%.3f,%lf");

  // Write the values generated by the probe.  The path that we
  // provide helps to disambiguate the source of the trace.
  fileHelper.WriteProbe ("ns3::DoubleProbe",
                         "/Names/Emitter/RemainingCpu",
                         "Output");
  FileHelper fileHelper2;

  // Configure the file to be written.
  fileHelper2.ConfigureFile ("mem2",
                            FileAggregator::FORMATTED);
  //fileHelper.ConfigureFile ("mem",
  //                          FileAggregator::FORMATTED);

  // Set the labels for this formatted output file.
//  fileHelper.Set2dFormat ("Time (Seconds) = %.3f\tCount = %lf");
  fileHelper2.Set2dFormat ("%.3f,%lf");

  // Write the values generated by the probe.  The path that we
  // provide helps to disambiguate the source of the trace.
  fileHelper2.WriteProbe ("ns3::DoubleProbe",
                         "/Names/Emitter/RemainingMemory",
                         "Output");
  //fileHelper.WriteProbe ("ns3::DoubleProbe",
  //                       "/Names/Emitter/RemainingMemory",
  //                       "Output");
    Simulator::Stop(Seconds(100000000));
    Simulator::Run();
    Simulator::Destroy();

    return 0;

}

