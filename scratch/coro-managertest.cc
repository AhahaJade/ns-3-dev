/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * his program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include"ns3/core-module.h"
#include"ns3/resource-unit.h"
#include"ns3/resource-unit-container.h"
#include"ns3/resource-unit-helper.h"
#include"ns3/object.h"
#include <iostream>
#include"ns3/coro-manager.h"
#include"ns3/manager-helper.h"
#include"ns3/stats-module.h"
#include<iostream>
#include<string>
#include<fstream>
#include<sstream>
#include"ns3/example-1.h"
#include"ns3/dcoro-task.h"

using namespace ns3;
void
TraceCpu (double oldValue, double newValue)
{
  std::cout<<"At time "<<Simulator::Now().As(Time::S) << " the old value of cpu  " << oldValue << " has changed to " << newValue << std::endl;
}
void
TraceMem (double oldValue, double newValue)
{
  std::cout <<"At time "<<Simulator::Now().As(Time::S)<< " the old value of memory  " << oldValue << " has changed to " << newValue << std::endl;
}
void
StringTraceNowId (std::string oldValue, std::string newValue)
{
  std::cout << "At time _s,the old value of NowId " << oldValue << " has changed to " << newValue << std::endl;
#include"ns3/stats-module.h"
}
void
StringTraceNowStatus (std::string oldValue, std::string newValue)
{
  std::cout << "At time _s,the old status of task " << oldValue << " has changed to " << newValue << std::endl;
}
int
main (int argc, char *argv[])
{

    Time::SetResolution (Time::NS);
    NodeContainer nodes;
    nodes.Create(3);
    /* 向指定node安装资源部件*/
    ResourceHelper res;
    res.Install(nodes.Get(2));
    Ptr<Node> node = nodes.Get(2);
    Ptr<ResourceContainer> container= node->GetObject<ResourceContainer>();
//    Ptr<Resource> re2 = CreateObject<Resource>();
//    container->Add(re2);
    Ptr<Resource> MyResource = *(container->Begin());
//    re2->TraceConnectWithoutContext ("RemainingMemory", MakeCallback (&TraceMem));
//    re2->TraceConnectWithoutContext ("RemainingCpu", MakeCallback (&TraceCpu));
    MyResource->TraceConnectWithoutContext ("RemainingMemory", MakeCallback (&TraceMem));
    MyResource->TraceConnectWithoutContext ("RemainingCpu", MakeCallback (&TraceCpu));
    /* 向指定node安装Manager*/
    ManagerHelper<CoroManager> manager;
    manager.Install(nodes.Get(2));
    auto no_manager = nodes.Get(2)->GetObject<CoroManager>();
    MyResource->SetResourceId("2020001");
    MyResource->SetMemory(0.2493);
    MyResource->SetCpu(0.5);
    
    Example1 example;
    Ptr<Example1::State> state = Create<Example1::State>();
    //Ptr<Task> dtask= Create<DCoroTask>(0.05,0.05);
    DCoroTask task = DCoroTask(0.05,0.05);
    task.StateInit(state);
    task.AddFunc(example.func1);
    task.AddFunc(example.func2);
    task.AddFunc(example.func3);
    //Ptr<Task> dtask = Create<Task>(*dynamic_cast<Task*>(&task));
    Task* dtask = dynamic_cast<Task*>(&task);
    //DCoroTask *temp = dynamic_cast<DCoroTask*>(GetPointer(dtask));
    DCoroTask *temp = dynamic_cast<DCoroTask*>(dtask);
    std::cout<<temp->m_funcChain.m_funcList.size()<<std::endl;

    no_manager->GenerateTask(dtask);

    no_manager->Start();
    
    Simulator::Stop(Seconds(100));
    Simulator::Run();
    Simulator::Destroy();

    return 0;

}

