#include"ns3/statistic-graph.h"
#include<iostream>
namespace ns3{
    
    void Point::Increase(size_t index)
    {
        m_arg[index]=m_arg[index]+1;
    }
    void Point::Decrease(int index)
    {
        --(m_arg[index]);
    }
    int Point::GetVSize() const
    {
        return 5;
    }
    int Point::GetArg(int index)
    {
        if(index<=GetVSize()-1)
        {
            return m_arg[index];
        }
        else
        {
            std::cerr<<"wrong Point index.";
            return -1;
        }
    }
    void Point::SetArg(int index,int value)
    {
        m_arg[index]=value;
    }

    void Graph::Statistic(int x,int y,int index)
    {
        m_matrix[x][y].Increase(index);
    }
    Point Graph::GetPosition(int x,int y)
    {
//        if(x<=m_sideLength-1&&y<=m_sideLength-1)
//        {
//            return m_matrix[x][y];
//        }
//        else
//        {
//            std::cerr<<"wrong Graph index.";
//            return;
//        }
        return m_matrix[x][y];
    }
    int Graph::GetSideLength() const
    {
        return m_sideLength;
    }
}
