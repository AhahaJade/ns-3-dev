#include"ns3/base-statistic.h"
#include"ns3/statistic-graph.h"
#include<fstream>
#include<sstream>
#include<iostream>
namespace ns3{
	class UndigraphStatistic:public StatisticBase{
	    public:
	        UndigraphStatistic(int row)
	        {
	            m_graph = new Graph(row);
	            m_switch = new std::string [row];
	        }
	        virtual ~UndigraphStatistic()
	        {
//                m_graph->~Graph();//问题出在这里
	            delete [] m_switch;
	        }
	        void SetListFile(std::string file);
	        void ReadListFile();
	        virtual void ReadFile() override;
	        void SaveData(std::string filePath) ;
	        bool LinkEnd(std::string switch_x,std::string switch_y);
            bool InvalidRow(std::string switch_x,std::string switch_y);
	
	    private:
	        void DoStatistic(int x,int y,int node_index);
	        int FindSwitchIndex(std::string switch_id);//读入的是switch_id,在统计时要将switch_id转化成矩阵坐标，输入到DoStatistic中。
	        Graph* m_graph;
	        std::string *m_switch;
	        std::string m_listFile;
	        
	};
	class FileProcessing{
	    public:
	        static void SaveFile(Graph graph,std::string filePath);
//            static Graph ReadFile(std::string file,int sideLength);//通过统计文件还原Graph
	    private:
	        void SetFilePath(std::string file);
	        std::string m_filePath;
	};
}
