//Point是Graph中的节点
#include<iostream>
#include<vector>
namespace ns3{
class Point{
    public:
        Point()=default;
        ~Point() = default;
        void Increase(size_t index);
        void Decrease(int index);
        int GetVSize() const;
        int GetArg(int index);
        void SetArg(int index,int value);
    private:
        int m_arg[5]={0,0,0,0,0};
};
//Graph是一个N*N的矩阵
class Graph{
    public:
        Graph(int row)
        {
            m_sideLength =row;
            m_matrix = new Point* [m_sideLength]; //这里得new
            for(int i=0;i!=m_sideLength;++i)
            {
                m_matrix[i] = new Point [m_sideLength];
            }
        }
        ~Graph()
        {
            for(int i=0;i!=m_sideLength;++i)
            {
                delete [] m_matrix[i];
            }
            delete [] m_matrix;
        }
        Point* operator[](int row)
        {
            return m_matrix[row];
        }
        void Statistic(int x,int y,int index);
        Point GetPosition(int x,int y);
        int GetSideLength() const;

    private:
        Point **m_matrix; //最后用于统计的矩阵
        int m_sideLength;

};
}
























