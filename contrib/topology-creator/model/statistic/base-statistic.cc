#include"ns3/base-statistic.h"
namespace ns3{
    void StatisticBase::SetFilePath(std::string file)
    {
        m_fileName = file;
    }
    std::string StatisticBase::GetFilePath() const
    {
        return m_fileName;
    }
}
