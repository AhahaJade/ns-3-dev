#include"ns3/task.h"
#include"ns3/ptr.h"
#include"ns3/receivebase.h"
#include"ns3/orchestratorbase.h"
namespace ns3{
    class ChatRoom{
        public:
            void join(Ptr<Object> object_1,Ptr<Object> object_2);
            void message1_2();//信息从pair.first发往pair.second
            void message2_1();//信息从pair.second发往pair.first
        private:
            std::pair<Ptr<OrchestratorBase>,Ptr<ReceiveBase>> chatlink;

    };
}
