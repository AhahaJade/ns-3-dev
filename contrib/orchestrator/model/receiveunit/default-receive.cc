#include"ns3/default-receive.h"
#include"ns3/log.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("DefaultReceive");
    NS_OBJECT_ENSURE_REGISTERED(DefaultReceive);
    bool DefaultReceive::ReceiveTask(Ptr<Task> task)//这一部分不在基类中声明是可能接收到的不是task而是一个job(由多个task组成)，接收的形式都大不一样
    {
        m_task = task;
        return HandleReceive();
    }
    bool DefaultReceive::ReceiveFromOthers(Ptr<Task> task)
    {
        NS_LOG_FUNCTION(this);
        auto taskId = task->GetTId();
        NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Accept tasks "<<taskId<<" forwarded in the network "<< std::endl);
        auto id = task->GetDestinationMId();
        if(m_receiveTable->GetPendingList().size()==2){
            return false;
        }
        //既然已经接受了，就不需要再对MId进行判断了
        m_receiveTable->GetTask(task);
        return true;
//        if(id==m_machineId)
//        {
//            m_receiveTable->GetTask(m_task);
//
//           // Simulator::ScheduleNow()
//            std::cout<<"needed to be ran"<<std::endl;
//            return true;
//        }
//        else
//        {
//            //传给app进行转发
//            std::cout<<"needed to be transfered";
//            return false;
//        }

    }
    bool DefaultReceive::HandleReceive(void) //这个判断还得改
    {
        auto id = m_task->GetDestinationMId();
        if(id==m_machineId)
        {
            m_taskStatusTable->GetTask(m_task);

           // Simulator::ScheduleNow()
            //std::cout<<"needed to be ran"<<std::endl;
            return true;
        }
        else
        {
            //传给app进行转发
            //std::cout<<"needed to be transfered";
            return false;
        }
    }

}
