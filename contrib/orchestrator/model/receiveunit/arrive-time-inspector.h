#include"ns3/inspector.h"
#include"ns3/task.h"
#include"ns3/ptr.h"
namespace ns3{
    class ArriveTimeInspector:public Inspector{ 
        ArriveTimeInspector() = default;
        ~ ArriveTimeInspector();
        Ptr<Task> operator()(Ptr<Task> t)
        {
	        Time dt = t->GetGenerateTime();
	        Time now = Now();
	        //manager在reveive之前先对任务的创建时间进行判断。假如创建时间小于当前时间，说明任务来得晚了，可能因为节点故障也可能因为链路拥堵。那么此时任务的执行策略是否应该发生变化？应该如何变化？（任务的优先级动态变化？任务再次进行转发？或者传输过程中对任务进行备份，超过一定时间就在备份节点上直接执行？）（这应该是可靠性方面的问题了吧）
	        if(dt<now)
	        { 
	            t->SetGenerateTime(0);//这其实就相当于我接受一个我就立马运行他
                return t;
	        }
	        else
	        {
	        //这里m_receiveUnit->ReceiveTask()到的task应该单独存放起来，与本地调度的任务区分开来。现在先receive一个就立即Schedule一个。
                Time generateTime = t->GetGenerateTime() - now;
                double time = generateTime.GetDouble();
                t->SetGenerateTime(time);
	            return t;
	        }

        }
    };
}
