#include"ns3/link-send.h"
#include"ns3/log.h"
#include"ns3/link-manager.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("LinkSend");
    NS_OBJECT_ENSURE_REGISTERED(LinkSend);

    TypeId LinkSend::GetTypeId(void)
    {
		static TypeId tid = TypeId("ns3::LinkSend")
		    .SetParent<Object>()
		    .SetGroupName("MySend");
		return tid;
    }

    void LinkSend::FindDestination(LinkTask task) 
    {
        NS_LOG_FUNCTION(this);

        //auto machineId = task.GetMachineList();//假如一个设备对应多个ip地址，那么应该发往哪一个呢
//        Time dt = task->GetGenerateTime();
//        if(m_machineId_IP.find(machineId) == m_machineId_IP.end())
//        {
//            NS_LOG_ERROR ("EasiEI:  At time " <<dt.As (Time::S) << " Machine: " <<m_machineId<<'\t'<< " Doesn't have corresponding IP address on the network " << std::endl);
//        }
//        else
//        {
//            Address adr = m_machineId_IP.find(machineId)->second;
//            TransferIpToApplication(task,adr);
//        }
        auto MachineList = task.GetMachineList();
        //if(MachineList.begin()==MachineList.end())
        if(MachineList.size()==1)
        {
            NS_LOG_INFO("EasiEI link scence : "<<task.GetTId()<<" attach the end point. ");
            Time now = Now();
            Time generateTime = task.GetGenerateTime();
            Time limit = Seconds(22);
//            std::cout<<task.m_generateTimeD<<std::endl;
            std::cout<<limit.GetSeconds()<<'\t'<<now.GetSeconds()<<'\t'<<generateTime.GetSeconds()<<'\t'<<(now-generateTime).GetSeconds()<<'\t'<<task.GetTime()<<std::endl;
            std::cout<<"--------------------"<<std::endl;

            //if((now - Seconds(task.GetTime())-Seconds(0.017303466796875))>Seconds(18))
            if(now - Seconds(task.GetTime())>Seconds(18))
            {
                LinkManager::Statistic(false);
            }
            else
            {
                LinkManager::Statistic(true);
            }
        }
        else
        {
            auto id = task.PopMachine();
            auto nextMachineId = MachineList.front();
            Address adr = m_machineId_IP[nextMachineId];
            TransferIpToApplication(task,adr);
        }
        



    }

    void LinkSend::Start()//Link client:设定总发送次数和仿真总时间，发送任务。需要保存一个LinkTask.  该函数负责调用link-client的Start
    {
//        auto client = DynamicCast<Client>(m_node->GetApplication(0));
//        client
        ;   
    }
    
    void LinkSend::SetMap(std::map<std::string,Address > map)
    {
        m_machineId_IP = map;
    }

    void LinkSend::TransferIpToApplication(LinkTask task,Address adr)
    {
        NS_LOG_FUNCTION(this);
        auto client = DynamicCast<LinkClient>(m_node->GetApplication(0));
        //这一步要动态建立socket
        client->SetRemote(adr,m_port);
        //task 链的转发，这一步之前要对输入输出比进行修改

        client->ReceiveTask(task);
    }
	        
}

