#include"ns3/default-send.h"
#include"ns3/log.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("DefaultSend");
    NS_OBJECT_ENSURE_REGISTERED(DefaultSend);

    TypeId DefaultSend::GetTypeId(void)
    {
		static TypeId tid = TypeId("ns3::DefaultSend")
		    .SetParent<Object>()
		    .SetGroupName("MySend");
		return tid;
    }

    void DefaultSend::FindDestination(Ptr<Task> task) 
    {
        NS_LOG_FUNCTION(this);
        std::string machineId = task->GetDestinationMId();//假如一个设备对应多个ip地址，那么应该发往哪一个呢
        Time dt = task->GetGenerateTime();
        if(m_machineId_IP.find(machineId) == m_machineId_IP.end())
        {
            NS_LOG_ERROR ("EasiEI:  At time " <<dt.As (Time::S) << " Machine: " <<m_machineId<<'\t'<< " Doesn't have corresponding IP address on the network " << std::endl);
        }
        else
        {
            Address adr = m_machineId_IP.find(machineId)->second;
            TransferIpToApplication(task,adr);
        }

    }

    void DefaultSend::Start()
    {
        NS_LOG_FUNCTION(this);
        if(m_sendTable->PendingEmpty())
        {
            NS_LOG_ERROR ("EasiEI:   Machine: " <<m_machineId<<'\t'<< " Doesn't have task to send " << std::endl);
            return ;
        }
        auto list = m_sendTable->GetPendingList();
        for(auto it = list.begin();it!=list.end();++it)
        {
            FindDestination(*it);
        }
        
    }
    
    void DefaultSend::SetMap(std::map<std::string,Address > map)
    {
        m_machineId_IP = map;
    }

    void DefaultSend::NewClient(Address adr,std::string machineId)
    {
        ClientHelper clent(adr,10);
        m_clientApps = clent.Install(GetNode());//因为每次添加新的application是push_back的，所以后续访问需要用m_clientNumber - index
        m_clientApps.Start(Seconds(m_startTime));
        m_clientApps.Stop(Seconds(m_endTime));
        m_machineId_Application.insert(std::pair<std::string,int>(machineId,m_clientNumber));
        m_clientNumber++;
    }

    void DefaultSend::SetClientTime(double start, double end)
    {
        m_startTime = start;
        m_endTime = end;
    }

    int DefaultSend::FindClientIndex(std::string machineId)
    {
        auto it = m_machineId_Application.find(machineId);
        if(it == m_machineId_Application.end())
        {
           // NS_LOG_ERROR ("EasiEI:  At time " <<dt.As (Time::S) << " Machine: " <<m_machineId<<'\t'<< " Doesn't have corresponding client applicaiton " << std::endl);
            return -1;
        }
        return it->second;
    }

    void DefaultSend::TransferIpToApplication(Ptr<Task> task,Address adr)
    {
        NS_LOG_FUNCTION(this);
        Time dt = task->GetGenerateTime();
        std::string machineId = task->GetDestinationMId();
        int index = FindClientIndex(machineId);
        if(index == -1)
        {
            NewClient(adr,machineId);
            index = FindClientIndex(machineId);
        }
        auto client = DynamicCast<Client>(m_node->GetApplication(m_clientNumber - index));
        //这一步要动态建立socket
        //client->SetRemote(adr,m_port);
        const Task t = *task;
        client->ReceiveTask(t,dt);
    }
	        
}

