#ifndef DEFAULTSEND_H
#define DEFAULTSEND_H
#include"ns3/object.h"
#include"ns3/ptr.h"
#include"ns3/type-id.h"
#include"ns3/node.h"
#include"ns3/traced-value.h"
#include"ns3/sendbase.h"
#include"ns3/task.h"
#include"ns3/ipv4-address.h"
#include"ns3/task-table.h"
#include"ns3/client.h"
#include"ns3/applications-module.h"
#include"ns3/application-helper.h"
#include"ns3/core-module.h"
namespace ns3{
    class Task;
    class TaskTable;
    class DefaultSend:public SendBase{
        public:
            static TypeId GetTypeId(void);
            DefaultSend(Ptr<TaskTable> sendTable,std::string machineId,Ptr<Node> node):SendBase(sendTable,machineId,node){}
            virtual void FindDestination(Ptr<Task> task) override;
            virtual void Start() override;
            void SetMap(std::map<std::string,Address> map);
            void NewClient(Address adr,std::string machineId);
            void SetClientTime(double start,double end);
            int FindClientIndex(std::string machienId);
        private:
            virtual void TransferIpToApplication(Ptr<Task> task,Address adr) override;
            std::map<std::string,Address> m_machineId_IP;//里面保存的是machineId与相应IP的对应关系
            std::map<std::string,int> m_machineId_Application;
            uint16_t m_port = 10;//要发往的端口，默认都为10
            double m_startTime;
            double m_endTime;
            ApplicationContainer m_clientApps;
            int m_clientNumber = 0; //记录总共有几个
    };

}
#endif /* DEFAULTSEND_H */
