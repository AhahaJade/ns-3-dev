#ifndef LINKSEND_H
#define LINKSEND_H
#include"ns3/object.h"
#include"ns3/ptr.h"
#include"ns3/type-id.h"
#include"ns3/node.h"
#include"ns3/traced-value.h"
#include"ns3/sendbase.h"
#include"ns3/task.h"
#include"ns3/ipv4-address.h"
#include"ns3/task-table.h"
#include"ns3/link-client.h"
/* 针对场景：单链路串行连接，A-->B-->C-->D，任务从任务经节点A发送，BCD处理
 * */
namespace ns3{
    class Task;
    class TaskTable;
    class LinkSend:public SendBase{
        public:
            static TypeId GetTypeId(void);
            LinkSend(Ptr<TaskTable> sendTable,std::string machineId,Ptr<Node> node):SendBase(sendTable,machineId,node){}
            void FindDestination(LinkTask task);
            virtual void FindDestination(Ptr<Task> task) override{
                ;
            }
            virtual void Start() override;
            void SetMap(std::map<std::string,Address> map);
        private:
            void TransferIpToApplication(LinkTask task,Address adr);
            virtual void TransferIpToApplication(Ptr<Task> task,Address adr) override
            {
                ;
            }
            std::map<std::string,Address> m_machineId_IP;//里面保存的是machineId与相应IP的对应关系
            uint16_t m_port = 10;//要发往的端口，默认都为10
    };

}
#endif /* LINKSEND_H */
