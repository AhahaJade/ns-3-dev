#ifndef TESTORCHESTRATOR_H
#define TESTORCHESTRATOR_H
#include"ns3/orchestratorbase.h"
#include"ns3/task.h"
namespace ns3{
    class Task;
    class TestOrchestrator:public OrchestratorBase{

        private:
            Ptr<Task> m_task;
            std::list<Ptr<Task>> m_taskRelease;
            std::list<Ptr<Task>> m_remoteRelease;
            std::map<std::string,std::string> m_taskAllocateTable;//记录相应task存放在resource container的哪个resource中，pair的first存放taskId作为关键字，因为本身一个task要被分配给一个resource。pair的second存放
            virtual void Release(Ptr<Task> task) override;
            virtual void ReleaseReceived(Ptr<Task> task) override;
        public:
            TestOrchestrator(Ptr<TaskTable> local,Ptr<TaskTable> remote,Ptr<TaskTable> send,Ptr<Node> node,std::string machineId):OrchestratorBase(local,remote,send,node,machineId){}
            virtual bool ChooseTask() override ;
            virtual bool ChooseReceivedTask() override ;
            Ptr<Task> ChooseSingleTask(size_t i);
            void StataTransfer(Ptr<Task> t,int i);
            void ReceivedStataTransfer(Ptr<Task> t,int i);
            virtual void Run(Ptr<Task> task) override;
            virtual void RunReceivedTask() override;
            void ReleaseResource(Ptr<Task> task,int i); //i==0时，释放本地Task资源，i==1时，释放网络中接收到的Task资源。
            void ReleaseInfoInsert();
            void ReleaseInfoDelete();
            void oneRoundDispatch ();
            void Transit (Time dt);
    };

};
#endif /* TESTORCHESTRATOR_H*/
