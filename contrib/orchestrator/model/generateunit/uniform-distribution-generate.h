#ifndef UNIFORMDISTRIBUTIONGENERATE_H
#define UNIFORMDISTRIBUTIONGENERATE_H
#include"ns3/generatetaskbase.h"
#include"ns3/task.h"
#include"ns3/task-table.h"
#include <initializer_list>
#include "boost/lexical_cast.hpp"
#include "ns3/core-module.h"
#include "ns3/client.h"
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include<iomanip>
namespace ns3{
    class Client;
    class DefaultGenerate:public GenerateTaskBase{
        public:
            static TypeId GetTypeId(void)
		    {
		        static TypeId tid = TypeId("ns3::DefaultGenerate")
		            .SetParent<Object>()
		            .SetGroupName("MyGenerate");
		        return tid;
		    }

            DefaultGenerate(Ptr<TaskTable> table,std::string Mid,Ptr<Node> node):GenerateTaskBase(table,Mid,node){}

            double GenerateRandomDouble(void)
            {   
                std::srand( (unsigned)time(0) );
				double rnd;
				rnd = (double) std::rand()/RAND_MAX * (m_upperrBound-m_lowerBound) + m_lowerBound;
                return rnd;
            }
            int GenerateRandomInt(void)
            {
                int lower=1;
				int upper=6;
				int md;
                std::srand((unsigned)time(NULL));
				md = (std::rand() % (upper-lower))+ lower;//[a,b)的随机整数
                return md;
            }
            virtual Ptr<Task> Generate() override
            {
               // auto now_orche = m_node.GetObject<OrchestratorBase>();
		        Task orinewtask(m_taskId,m_requestcpu,m_requestmem,m_prior,m_destinationMachineId);
		        //Ptr<Task> newtask = CreateObject<Task>(orinewtask);
		        Ptr<Task> newtask = Create<Task>(m_taskId,m_requestcpu,m_requestmem,m_prior,m_destinationMachineId);
                newtask->SetGenerateTime(Seconds(m_time));
		        newtask->ChangeOriginMId(m_machineId);
		        if(!m_destinationMachineId.empty())
		        {
		            //newtask->ChangeDestinationMId(destinationMachineId);
		            //newtask->Unsubmitted();
                    //这里直接调用继续进行转发，但是逻辑还理不太清。目前想法是直接把task传给app，直接发送然后跳出这个函数。
                    //m_taskStatusTable->Sending(newtask);
//                    const Task task = *newtask;
//                    auto client = DynamicCast<Client>(m_node->GetApplication(0));
//                    client->ReceiveTask(task);
                    return newtask;
		        }
		        else
		        {
		            newtask->ChangeDestinationMId(m_machineId);
               //     当目标地址为空时，发送目标就为本机地址，直接把任务加到本机的pending表
		        }
		        newtask->ChangeNowMId(m_machineId);
                m_taskStatusTable->GetTask(newtask);
		        //if(destinationMachineId.empty())
		        //{
		        //    ReceiveTask(newtask);
		        //}
		        return newtask;
            }
            virtual void GetInfo() override
            {
                m_taskId = boost::lexical_cast<std::string>(id++);
                m_requestcpu = 1;
                m_requestmem = 1;
                m_prior = GenerateRandomInt();
                m_time = GenerateRandomDouble();
            }
            virtual void GetInfo(std::initializer_list<std::string> taskInfo) override
            {
                ;
            }
            int GetTaskNumber(void) const
            {
                return m_taskNumber;
            }
            double GetTime(void) const
            {
                return m_time;
            }
        private:
            static double m_id = 10000;
            double m_lowerBound=0;
            double m_upperrBound = 10;
            int m_taskNumber = 100;//表示生成任务的数量
            std::string m_taskId;
            double m_requestcpu;
            double m_requestmem;
            std::size_t m_prior;
            std::string m_destinationMachineId = "";
            double m_time;
    };
}
#endif /*  UNIFORMDISTRIBUTIONGENERATE_H    */
