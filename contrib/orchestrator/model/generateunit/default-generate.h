#ifndef DEFAULTGENERATE_H
#define DEFAULTGENERATE_H
#include"ns3/generatetaskbase.h"
#include"ns3/task.h"
#include"ns3/task-table.h"
#include <initializer_list>
#include "boost/lexical_cast.hpp"
#include "ns3/core-module.h"
#include "ns3/client.h"
#include <iostream>
#include <boost/random.hpp>
#include <boost/random.hpp>
#include<vector>
#include"ns3/resource-unit.h"
namespace ns3{
    class Client;
    class DefaultGenerate:public GenerateTaskBase{
        public:
            static TypeId GetTypeId(void);

            DefaultGenerate(Ptr<TaskTable> tasktable,Ptr<TaskTable> sendtable,std::string Mid,Ptr<Node> node):GenerateTaskBase(tasktable,sendtable,Mid,node){}
            static double GenerateRandomDouble(void);
            static int GenerateRandomInt(void);

            virtual Ptr<Task> Generate() override;
            virtual void GetInfo(std::initializer_list<std::string> taskInfo) override;
            std::vector<Ptr<Task>> ContinuousDiscreteDistributions();
            std::vector<Ptr<Task>> NormalDistribution() ;
            int GetTaskNumber(void) const;
            double GetTime(void) const;
            std::vector<Ptr<Task>> m_task;
        private:
            static unsigned int m_seed;
            static int m_id;
            static double m_lowerBound;
            static double m_upperrBound;
            int m_taskNumber = 10;//表示生成任务的数量
            std::string m_taskId;
            double m_requestcpu;
            double m_requestmem;
            std::size_t m_prior;
            std::string m_destinationMachineId = "";
            double m_startTime;
            double m_endTime;
    };
}
#endif /*  DEFAULTGENERATE_H    */
