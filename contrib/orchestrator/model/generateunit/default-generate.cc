#include"default-generate.h"
#include"ns3/log.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("DefaultGenerate");
    NS_OBJECT_ENSURE_REGISTERED(DefaultGenerate);
    
    int DefaultGenerate::m_id = 10000;
    double DefaultGenerate::m_lowerBound = 0;
    double DefaultGenerate::m_upperrBound =1;

    TypeId DefaultGenerate::GetTypeId(void)
    {
		static TypeId tid = TypeId("ns3::DefaultGenerate")
		    .SetParent<Object>()
		    .SetGroupName("MyGenerate");
		return tid;
    }
    double DefaultGenerate::GenerateRandomDouble(void)
    {   
        boost::mt19937 gen;
        boost::uniform_real<>dist(m_lowerBound,m_upperrBound);
        boost::variate_generator<boost::mt19937&,boost::uniform_real<>>die(gen,dist);
        return die();
    }
    int DefaultGenerate::GenerateRandomInt(void)
    {
        int lower=1;
		int upper=6;
		int md;
        std::srand((unsigned)time(0));
		md = (std::rand() % (upper-lower))+ lower;//[a,b)的随机整数
        return md;
    }
    Ptr<Task> DefaultGenerate::Generate() 
    {
               // auto now_orche = m_node.GetObject<OrchestratorBase>();
        NS_LOG_FUNCTION(this);
        NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Start creating the task "<<m_taskId << std::endl);
        Task orinewtask(m_taskId,m_requestcpu,m_requestmem,m_prior,m_destinationMachineId);
        //Ptr<Task> newtask = CreateObject<Task>(orinewtask);
        Ptr<Task> newtask = Create<Task>(m_taskId,m_requestcpu,m_requestmem,m_prior,m_destinationMachineId);
        newtask->SetGenerateTime(m_startTime);

        newtask->SetEndTime(m_endTime);
        newtask->ChangeOriginMId(m_machineId);
        NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Task "<<m_taskId<<" creation complete " << std::endl);
        if(m_destinationMachineId!=m_machineId)
        {
            //newtask->ChangeDestinationMId(destinationMachineId);
            //newtask->Unsubmitted();
                    //这里直接调用继续进行转发，但是逻辑还理不太清。目前想法是直接把task传给app，直接发送然后跳出这个函数。
                    //m_taskStatusTable->Sending(newtask);
//                    const Task task = *newtask;
//                    auto client = DynamicCast<Client>(m_node->GetApplication(0));
//                    client->ReceiveTask(task);
            m_sendTable->GetTask(newtask);
            return newtask;
        }
        else
        {
            newtask->ChangeNowMId(m_machineId);
            m_taskStatusTable->GetTask(newtask);
            return newtask;
            //newtask->ChangeDestinationMId(m_machineId);
               //     当目标地址为空时，发送目标就为本机地址，直接把任务加到本机的pending表
        }
        
        //if(destinationMachineId.empty())
        //{
        //    ReceiveTask(newtask);
        //}
    }
    void DefaultGenerate::GetInfo(std::initializer_list<std::string> taskInfo) 
    {
        NS_LOG_FUNCTION(this);
        auto it = taskInfo.begin();
        m_taskId = *(it++);
        NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Accepts the task " <<m_taskId<<" information"<< std::endl);
        m_requestcpu =boost::lexical_cast<double>(*it++);
        m_requestmem = boost::lexical_cast<double>(*it++);
        m_prior = boost::lexical_cast<std::size_t>(*it++);
        m_destinationMachineId = *(it++);
        m_startTime = boost::lexical_cast<double>(*it++); //默认单位：秒
        m_endTime = boost::lexical_cast<double>(*it);
        NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Task: "<<m_taskId<<"  information received completed" << std::endl);
    }
    std::vector<Ptr<Task>> DefaultGenerate::ContinuousDiscreteDistributions() 
    {
        boost::mt19937 gen;
        boost::uniform_real<>resource(m_lowerBound,m_upperrBound);
        boost::variate_generator<boost::mt19937&,boost::uniform_real<>>die_d(gen,resource);
        boost::uniform_real<>time(0,10);
        boost::variate_generator<boost::mt19937&,boost::uniform_real<>>die_time(gen,time);
        boost::uniform_int<>prior(1,9);
        boost::variate_generator<boost::mt19937&,boost::uniform_int<>>die_i(gen,prior);
        for(int i =0;i!=m_taskNumber;++i)
        {
		    m_taskId = boost::lexical_cast<std::string>(m_id++);
		    m_requestcpu = die_d();
		    m_requestmem = die_d();
		    m_prior = die_i();
		    m_startTime = die_time();
            m_endTime = m_startTime +2;//待完善
            m_destinationMachineId = "";
            auto task = Generate();
            auto it = find_if(m_task.begin(),m_task.end(),[task](Ptr<Task> a){return  task->GetGenerateTime()<a->GetGenerateTime();});
            m_task.insert(it,task);
        }
        return m_task;
    }
    std::vector<Ptr<Task>> DefaultGenerate::NormalDistribution() 
    {
        boost::mt19937 gen;
        boost::normal_distribution<>time(m_lowerBound,m_upperrBound);
        boost::variate_generator<boost::mt19937&,boost::normal_distribution<>>die_d(gen,time);
        boost::uniform_int<>prior(1,9);
        boost::variate_generator<boost::mt19937&,boost::uniform_int<>>die_i(gen,prior);
        for(int i =0;i!=m_taskNumber;++i)
        {
		    m_taskId = boost::lexical_cast<std::string>(m_id++);
		    m_requestcpu = die_d();
		    m_requestmem = die_d();
		    m_prior = die_i();
		    m_startTime = die_d();
            auto task = Generate();
            m_task.push_back(task);
        }
        return m_task;
    }
    int DefaultGenerate::GetTaskNumber(void) const
    {
        return m_taskNumber;
    }
    double DefaultGenerate::GetTime(void) const
    {
        return m_startTime;
    }

}
