#include"ns3/link-manager.h"
#include "ns3/link-server.h"
#include "ns3/link-client.h"
#include <time.h>
#include<fstream>
#include<sstream>
#include<iostream>
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("LinkManager");
    NS_OBJECT_ENSURE_REGISTERED(LinkManager);

    std::pair<long,long> LinkManager::m_succeedFail = std::make_pair(0,0);
    
    TypeId LinkManager::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::LinkManager")
            .SetParent<Object>()
            .SetGroupName("MyLinkManager")
            .AddConstructor<LinkManager>();
            
        return tid;
    }
    void LinkManager::InitialScene(LinkTask sample,double totalTime,double send)
    {
        m_clientInit = std::make_tuple(sample,totalTime,send);
        //Simulator::Schedule(Seconds(totalTime*2),&LinkManager::PrintResult,this);
        
        Simulator::Schedule(Seconds(totalTime*2),&LinkManager::SaveResult,this,"/home/ssf/tarballs/EasiEI/ns-3-dev/scratch/result.csv");
    }
    void LinkManager::Start(void)
    {
        //Start初始化，再调用Run，通过传递的参数来区分自己是第一个节点还是其他节点
        double interval = std::get<1>(m_clientInit)/std::get<2>(m_clientInit);
//        if(m_node->GetNApplications()>=2)
//        {
//            auto client = DynamicCast<LinkClient>(m_node->GetApplication(0));
//            client->SetTotalTime(std::get<1>(m_clientInit));
//            client->SetSendTime(std::get<2>(m_clientInit));
//            Simulator::Schedule(Seconds(0),&LinkManager::RunTask,this,std::get<0>(m_clientInit));
//        }
        for(double time =0;time!=std::get<1>(m_clientInit);time+=interval)
        {
            auto temp = std::get<0>(m_clientInit);
            auto cpu = temp.GetRCpu();
            LinkTask task(cpu,0,{"1000","2000","3000","4000"});
            task.SetTime(time);
//            std::cout<<task.m_generateTimeD<<std::endl;   LinkServer的反序列化有问题
//            std::cout<<task.GetGenerateTime()<<std::endl;
            Simulator::Schedule(Seconds(time),&LinkManager::RunTask,this,task);
        }
    }

    void LinkManager::ReceiveTask(LinkTask t)//这个是跟server的接口
    {
        RunTask(t);

    }
    void LinkManager::RunTask(LinkTask task)
    {
        double probablity = rand() / double(RAND_MAX);
        
//        double probablity = (1+rand()%10)/10.0;
        auto resource = find_if(m_resourceDistribution.begin(),m_resourceDistribution.end(),[probablity](std::pair<double,double> resource_pair){ return probablity<resource_pair.first;});
//        std::cout<<probablity<<std::endl;
//        std::cout<<resource->first<<std::endl;
//        std::cout<<resource->second;
        if(task.GetMachine()=="1000")
        {
        //double time = (task.GetRCpu())/(resource->second);
        //task.SetGenerateTime(time);
        //std::cout<<time;
        //Simulator::Schedule(Seconds(time),&LinkManager::SendTask,this,task);
        Simulator::Schedule(Seconds(0),&LinkManager::SendTask,this,task);
        }
        else if(task.GetMachine()=="2000")
        {
        double time = (task.GetRCpu()*0.8)/(resource->second);
        std::cout<<"Machine 2000: run "<<time<<"s"<<std::endl;
        Simulator::Schedule(Seconds(time),&LinkManager::SendTask,this,task);
        }
        else if(task.GetMachine()=="3000")
        {
        double time = (task.GetRCpu()*0.8*1.2)/(resource->second);
        std::cout<<"Machine 3000: run "<<time<<"s"<<std::endl;
        Simulator::Schedule(Seconds(time),&LinkManager::SendTask,this,task);
        }
        else if(task.GetMachine()=="4000")
        {
        //double time = (task.GetRCpu()*0.8*1.2)/(resource->second);
//        Simulator::Schedule(Now(),&LinkManager::SendTask,this,task);
          SendTask(task);
        }
        else{
            ;
        }


        
    }
    void LinkManager::SendTask(LinkTask task)
    {
        m_sendUnit->FindDestination(task);
    }

    void LinkManager::SetNode(Ptr<Node> node)
    {
        m_node = node;
        m_sendUnit = Create<LinkSend>(m_sendTable,m_machineId,m_node);
    }
    Ptr<Node> LinkManager::GetNode(void) const
    {
        return m_node;
    }
    void LinkManager::SetMachineId(const std::string& Id)
    {
        m_machineId = Id;//还没改完，部件内的Id也要改
        m_sendUnit->SetMachineId(Id);
        if(m_node->GetNApplications()>=2)
        {
            auto server = DynamicCast<LinkServer>(m_node->GetApplication(1));
            server->SetMachineId(Id);
            auto client = DynamicCast<LinkClient>(m_node->GetApplication(0));
            client->SetMachineId(Id);
        }
    }
    void LinkManager::SetMap(std::map<std::string,Address> DNS)
    {
        m_sendUnit->SetMap(DNS);
    }
    void LinkManager::SetResourceDis(std::map<double,double> dis)
    {
        m_resourceDistribution = dis;
//        for(auto i:m_resourceDistribution)
//        {
//            std::cout<<i.first<<'\t'<<i.second;
//            std::cout<<std::endl;
//        }
    }
    void LinkManager::PrintResult()
    {
        std::cout<<"成功次数: "<<m_succeedFail.first<<'\t'<<"失败次数: "<<m_succeedFail.second<<std::endl;
    }
    void LinkManager::SaveResult(std::string filePath)
    {
        std::ofstream out;
        out.open(filePath,std::ios::app);
        if(out.is_open())
        {
	        out<<m_succeedFail.first<<","<<m_succeedFail.second<<','<<m_succeedFail.first/double(m_succeedFail.first+m_succeedFail.second);
            out<<"\n";
        }
        out.close();
    }
    

} 
