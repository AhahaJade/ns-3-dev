#include"task-table.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("TaskTable");
    NS_OBJECT_ENSURE_REGISTERED(TaskTable);
    void TaskTable::GetTask(Ptr<Task> t)
    {
        InsertByPriority(t);
    }
    void TaskTable::Print(void) const
    {
        std::cout<<"UnsubmitList information"<<std::endl;
        Printlist(m_unsubmitList);
        std::cout<<"PendingList information"<<std::endl;
        Printlist(m_pendingList);
        std::cout<<"RunningList information"<<std::endl;
        Printlist(m_runningList);
        std::cout<<"DeadList information"<<std::endl;
        Printlist(m_deadList);
    }
    bool TaskTable::CheckIn(Ptr<Task> t) const
    {
        auto it = find_if(m_pendingList.begin(),m_pendingList.end(),[t](Ptr<Task> a){return a->GetTId()==t->GetTId();});
        if(it == m_pendingList.end())
        {
            //std::cout<<"Task dosen't exist in this machine's Pending list,you can't schedule it "<<std::endl;
            //abort();
            return false;//表示当前无可执行task
        }
        return true;
    }
//    void TaskTable::CheckStataCorrect(Ptr<Task> t) const
//    {
//        if(t->GetCategoryInt() != 2)
//        {
//            std::cout<<"You can only change pending to running."<<std::endl;
//            abort();
//        }
//    }
    void TaskTable::PendingToRunning(Ptr<Task> t)
    {
        auto it = find_if(m_pendingList.begin(),m_pendingList.end(),[t](Ptr<Task> a){return a->GetTId()==t->GetTId();});
//        if(it == m_pendingList.end())
//        {
//            std::cout<<"Task doesn't exist "<<std::endl;
//            abort();
//        }
        auto temp = *it;
        //m_unsubmitList.erase(it);
        (*it)->Running();
        InsertByPriority(*it);
        m_pendingList.erase(it);

    }
    void TaskTable::RunningToDead(Ptr<Task> t)
    {
        auto it = find_if(m_runningList.begin(),m_runningList.end(),[t](Ptr<Task> a){return a->GetTId()==t->GetTId();});
        if(it == m_pendingList.end())
        {
            std::cout<<"Task doesn't exist "<<std::endl;
//            abort();
        }
        (*it)->Dead();
        InsertByPriority(*it);
        m_runningList.erase(it);
    }
    void TaskTable::RunningToPending(Ptr<Task> t)
    {
        auto it = find_if(m_runningList.begin(),m_runningList.end(),[t](Ptr<Task> a){return a->GetTId()==t->GetTId();});
//        if(it == m_pendingList.end())
//        {
//            std::cout<<"Task doesn't exist "<<std::endl;
//            abort();
//        }
        auto temp = *it;
        //m_unsubmitList.erase(it);
        (*it)->Pending();
        InsertByPriority(*it);
        m_runningList.erase(it);

    }
    void TaskTable::Printlist(const std::list<Ptr<Task>> &l) const
    {
        std::cout<<"TaskId"<<'\t'<<"cpu"<<'\t'<<"mem"<<'\t'<<"priority"<<'\t'<<"category"<<'\t'<<"time"<<std::endl;
        for(auto i = l.begin();i!=l.end();++i)
        {
            std::cout<<(*i)->GetTId()<<'\t'<<(*i)->GetRCpu()<<'\t'<<(*i)->GetRMemory()<<'\t'<<(*i)->GetPriority()<<'\t'<<'\t'<<(*i)->GetCategoryString()<<"\t"<<(*i)->GetGenerateTime()<<std::endl;
        }
    }
    Ptr<Task> TaskTable::FirstPriorInPending(void)
    {
        return *m_pendingList.begin();
    }
    void TaskTable::DeletePendingFirst(void)
    {
        m_pendingList.pop_front();
    }
    std::list<Ptr<Task>>& TaskTable::GetPendingList(void) const
    {
        return m_pendingList;
    }
    std::list<Ptr<Task>>& TaskTable::GetRunningList(void) const
    {
        return m_runningList;
    }
    bool TaskTable::PendingEmpty(void)
    {
        return m_pendingList.empty();
    }
//    void TaskTable::Sending(Ptr<Task> t)
//    {
//        m_sendList.push_back(t);
//    }
    void TaskTable::InsertByPriority(Ptr<Task> t)
    {
        std::list<Ptr<Task>> *l = nullptr;
        int cate = t->GetCategoryInt();
        switch(cate)
        {
            case 1:
                {
                    l = &m_unsubmitList;
                    break;
                }
            case 2:
                {
                    l = &m_pendingList;
                    break;
                }
            case 3:
                {
                    l = &m_runningList;
                    break;
                }
            case 4:
                {
                    l = &m_deadList;

//                    auto it = find_if(l->begin(),l->end(),[t](Ptr<Task> a){return  a->GetEndTime()<t->GetEndTime();});
//                    l->insert(it,t);
//                    l->sort(Compare);
                    break;
                }
            default:
                {
                    std::cout<<"Error: Task status doesn't match any category"<<std::endl;
                    //abort();
                }
        }
//        if(l == &m_pendingList)
//        {
//            auto it = find_if(l->begin(),l->end(),[dt](auto a){return  dt>a.second;});
//            auto temp = std::make_pair(t,dt);
//            l->insert(it,temp);
//            for(auto it_2 = l.begin(),auto it_3 = ++it_2;it!=l.end();++it_2,++it_3)
//            {
//                if(*(it_2).second==*(it_3).second)
//                {
//	                while(*(it_2).second == *(it_3).second)
//	                {
//	                    ++it_3;
//	                }
//                    std::sort(it_2,it_3-1,[](std::pair<Ptr<Task>,Time>> a,std::pair<Ptr<Task>,Time>> b){return (a.fist)->GetPriority()<(b.first)->GetPriority() });
//                    it_2 = it_3;
//                    it_3=it_2+1;
//                }
//            }
//        }
        auto it = find_if(l->begin(),l->end(),[t](Ptr<Task> a){return  a->GetGenerateTime()<t->GetGenerateTime();});
        l->insert(it,t);
        l->sort(Compare);
    }
    bool TaskTable::Compare(Ptr<Task> a,Ptr<Task> b)
    {
	        if(a->GetGenerateTime()==b->GetGenerateTime())
	        {
	            return a->GetPriority()<b->GetPriority();
	        }
	        else
	        {
	            return a->GetGenerateTime()<b->GetGenerateTime();
	        }
    }
    Ptr<TaskTable> TaskTable::GetReceiveTable(void) const
    {
        return m_receiveTable;
    }
}

