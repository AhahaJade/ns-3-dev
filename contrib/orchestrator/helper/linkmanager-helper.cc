/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "linkmanager-helper.h"
#include"ns3/config.h"
#include"ns3/names.h"
#include<iostream>

namespace ns3 {

    LinkManagerHelper::~LinkManagerHelper(){}
    void LinkManagerHelper::Install(Ptr<Node> node) const
    {
        Install(NodeContainer(node));
    }
    void LinkManagerHelper::Install(NodeContainer c) const
    {
        for(NodeContainer::Iterator i= c.Begin();i!=c.End();++i)
        {
            Ptr<LinkManager> src = DoInstall(*i);
            Ptr<LinkManager> ManagerOnNode = (*i)->GetObject<LinkManager>();
            if(ManagerOnNode == NULL)
            {
                (*i)->AggregateObject(src);
            }
            else
            {
                std::cout<<"Code goes wrong, each node can only have one orchestrator"<<std::endl;
            }
        }

    }

    /*     private  part        */
    Ptr<LinkManager> LinkManagerHelper::DoInstall(Ptr<Node> node) const
    {
        NS_ASSERT(node!=NULL);
        ObjectFactory fac;
        fac.SetTypeId("ns3::LinkManager");
        Ptr<LinkManager> manager = fac.Create<LinkManager>();
        NS_ASSERT(manager!=NULL);
        manager->SetNode(node);
        return manager;
    }

}

