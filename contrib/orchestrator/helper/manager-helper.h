/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef MANAGER_HELPER_H
#define MANAGER_HELPER_H

#include "ns3/manager.h"
#include"ns3/attribute.h"
#include"ns3/object-factory.h"
#include"ns3/node-container.h"
#include"ns3/ptr.h"
#include"ns3/net-device.h"
#include"ns3/net-device-container.h"

namespace ns3 {
    template <typename T>
    class ManagerHelper{

        public:
            ~ManagerHelper();
            void Install(Ptr<Node> node) const;
            void Install(NodeContainer c) const;

        private:
            Ptr<T> DoInstall(Ptr<Node> node) const;
    };
    template <typename T>
    ManagerHelper<T>::~ManagerHelper(){}
    template <typename T>
    void ManagerHelper<T>::Install(Ptr<Node> node) const
    {
        Install(NodeContainer(node));
    }
    template <typename T>
    void ManagerHelper<T>::Install(NodeContainer c) const
    {
        for(NodeContainer::Iterator i= c.Begin();i!=c.End();++i)
        {
            Ptr<T> src = DoInstall(*i);
            Ptr<T> ManagerOnNode = (*i)->GetObject<T>();
            if(ManagerOnNode == NULL)
            {
                (*i)->AggregateObject(src);
            }
            else
            {
                std::cout<<"Code goes wrong, each node can only have one orchestrator"<<std::endl;
            }
        }

    }

    /*     private  part        */
    template <typename T>
    Ptr<T> ManagerHelper<T>::DoInstall(Ptr<Node> node) const
    {
        // std::string str(TRANS(T));
        // std::cout << str << std::endl;
        NS_ASSERT(node!=NULL);
        ObjectFactory fac;
        fac.SetTypeId(T::GetTypeId());
        Ptr<T> manager = fac.Create<T>();
        NS_ASSERT(manager!=NULL);
        manager->SetNode(node);
        return manager;
    }
}

#endif /* ORCHESTRATOR_HELPER_H */


