#include <concepts>
#include <coroutine>
#include <exception>
#include <iostream>

struct ReturnObject {
  struct promise_type {
    ReturnObject get_return_object() { return {}; }
    std::suspend_never initial_suspend() { return {}; }
    std::suspend_never final_suspend() noexcept { return {}; }
    void unhandled_exception() {}
  };
};

//通过Awaiter来实现不同的挂起策略，来判断任务的起止
struct Awaiter {
  std::coroutine_handle<> *hp_;
  constexpr bool await_ready() const noexcept {
    
      return false; 
  }

  void await_suspend(std::coroutine_handle<> h) { 
      std::cout<<"suspend"<<std::endl;
      *hp_ = h; 
  }
  constexpr void await_resume() const noexcept {}
  int t = 3;
};

//counter本质上就是实现了一个调度方案，调度方案针对每个handle处理。
//可以通过Awaiter为每个handle进行赋能，来保证不同的挂起唤醒策略。
ReturnObject
counter(std::coroutine_handle<> *continuation_out)
{
  Awaiter a{continuation_out};
  for (unsigned i = 0;; ++i) {
    std::cout << "counter: " << i << std::endl;
    co_await a;
  }
}

void
main1()
{
  std::coroutine_handle<> h;
  counter(&h); //这里构造的时候也调用了一次函数。
  for (int i = 0; i < 3; ++i) {
    std::cout << "In main1 function\n";
    h();
  }
  h.destroy();
}
struct ReturnObject2 {
  struct promise_type {
    ReturnObject2 get_return_object() {
      return {
        // Uses C++20 designated initializer syntax
        .h_ = std::coroutine_handle<promise_type>::from_promise(*this)
      };
    }
    std::suspend_never initial_suspend() { return {}; }
    std::suspend_never final_suspend() noexcept { return {}; }
    void unhandled_exception() {}
  };

  std::coroutine_handle<promise_type> h_;
  operator std::coroutine_handle<promise_type>() const { return h_; }
  // A coroutine_handle<promise_type> converts to coroutine_handle<>
  operator std::coroutine_handle<>() const { return h_; }
};

ReturnObject2
counter2()
{
  for (unsigned i = 0;; ++i) {
    std::cout << "counter2: " << i << std::endl;
    co_await std::suspend_always{};
  }
}

void
main2()
{
  std::coroutine_handle<> h = counter2();
  //绑定的时候仍然会跑一次
  for (int i = 0; i < 3; ++i) {
    std::cout << "In main2 function\n";
    h();
  }
  h.destroy();
}
//int main(){
//    main2();
//    return 0;
//}
