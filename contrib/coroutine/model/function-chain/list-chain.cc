#include"ns3/list-chain.h"
namespace ns3{
    
    /*
     * 这里只负责执行当前it所指向的function，it的移动与末尾
     * 判断、it初始化都不在这里体现
     *
     * 如果上一次没执行完则继续执行，如果上一个执行完了就执行下一个，如果到末尾了就退出
     *
     * 是否连续执行以及时间戳的记录属于同步模块
     * */
    bool ListChain::Continue(){  
        if(m_init == false)
        {
           std::cout<<"1"<<std::endl;
           std::coroutine_handle<> coro = (*m_it)(m_state);
           m_handle.destroy();
           m_handle = coro;
           m_init = true;
           m_handle.resume();
           if(m_handle.done()){
               m_it++; 
               m_init = false;
           }
           return true;
        }
        //else if(!m_handle.done()){
        else{
            std::cout<<"2"<<std::endl;
            m_handle.resume();
            if(m_handle.done()){
                m_it++; 
                m_init = false;
            }
            return true;
        }
//        else{
//            std::cout<<"Error"<<std::endl;
//            std::abort();
//        }
    }

    void ListChain::AddFunc(std::function<DefaultCoro(Ptr<StateBase>)> func){
        if(m_funcList.begin() == m_funcList.end()){
            m_funcList.push_back(func);
            m_it = m_funcList.begin();
            m_handle = (*m_it)(m_state);
        }
        else{
            m_funcList.push_back(func);
        }
    }
    
    void ListChain::UpdateState(Ptr<StateBase> state){
        m_state = state;
    }

//    std::vector<char*> ListChain::Serialize(){
//        std::vector<char*> res(m_funcList.size());
//        for(auto funcIt = m_funcList.begin(); funcIt != m_funcList.end(); ++funcIt){
//            auto func = *funcIt;
//            if( func ) // if the call-wrapper has wrapped a callable object
//            {
//                //DefaultCoro* function_t( Ptr<StateBase> ) ;
//                auto function_t = *func.target<DefaultCoro(Ptr<StateBase>())> ;
//                if( function_t != nullptr )
//                {
//                    // the call-wrapper has wrapped a free function
//                    // and ptr_fun is a pointer to that function
//                    // use ptr_fun
//                    std::cout<<"成功"<<std::endl;
//                }
//                else
//                {
//                    
//                    // the callable object wrapped by the call-wrapper is not a free function of this type
//                    // it is a closure, a function object, a bind expression, or a free function with a
//                    // different (though compatible) type - for example: int* function_type( const void* ) ;
//                    std::cout<<"失败"<<std::endl;
//
//                }
//            }
//        }
//        return res;
//    }

    bool ListChain::Done(){
        return m_it == m_funcList.end();
    }
}

