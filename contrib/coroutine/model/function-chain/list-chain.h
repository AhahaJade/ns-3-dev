#ifndef LISTCHAIN_H
#define LISTCHAIN_H
#include"ns3/object.h"
#include"ns3/core-module.h"
#include"ns3/ptr.h"
#include<list>
#include<functional>
#include<coroutine>
#include"ns3/default-coro.h"
#include"ns3/chain-base.h"
#include<vector>
#include <boost/serialization/serialization.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
namespace ns3{
    /*
     * 中间的同步全由协程的init_suspend和final_suspend完成，
     * 因此这里不需要留出同步的操作。
     *
     * 但是此处需要记录同步的总信息，要一直和子协程更新信息。
     * 这里同步是需要对任务信息记录，所以保存一个Ptr<Task>,
     * 并且更新的接口也留作Ptr<Task>
     *
     * 那么chain和Task的关系是什么，毕竟调度的时候需要传入一个task对象
     * 所以chain是task的一个元素，作为task的友元
     * */
    class ListChain : public ChainBase{
        public:
            ListChain() = default;
            virtual bool Continue();
            void AddFunc(std::function<DefaultCoro(Ptr<StateBase>)>);
            void UpdateState(Ptr<StateBase>);
            std::vector<char*> Serialize();
            bool Done();
            template<class Ar> void Serialize(Ar &ar, const unsigned int version){
                ar & m_funcList;
            }
            void Print(){
                std::cout<<m_funcList.size()<<"!!!!!!!!!!!"<<std::endl;
            }
            //virtual bool Record(Task *) = 0; 这一部放在state machine来做吧
        private:
            //m_funcList和m_state需要序列化和反序列化操作。
            std::list<std::function<DefaultCoro(Ptr<StateBase>)>> m_funcList;
            Ptr<StateBase> m_state;
            std::list<std::function<DefaultCoro(Ptr<StateBase>)>>::iterator m_it ;
            std::coroutine_handle<> m_handle ;
            bool m_init = false;

    };
}
#endif /* LISTCHAIN_H */
