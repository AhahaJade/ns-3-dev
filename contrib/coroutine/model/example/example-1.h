/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef EXAMPLE1_H
#define EXAMPLE1_H
#include"ns3/task.h"
#include"ns3/default-coro.h"
#include"ns3/state-base.h"
#include"ns3/dcoro-task.h"
#include<coroutine>
#include"ns3/type-id.h"
#include"ns3/dcoro-task.h"
#include"ns3/object.h"
namespace ns3 {
    class DCoroTask;
    struct Example1 : public Object {

        Example1() = default;
        static TypeId GetTypeId(void);
        struct State : public StateBase{
            void StateTransfer(Ptr<StateBase> s);
            int a = 1;
            int b = 2;
            double res = 0;
        };
        
        void InitTask(DCoroTask &task);
        static DefaultCoro func1(Ptr<StateBase> s);
        static DefaultCoro func2(Ptr<StateBase> s);
        static DefaultCoro func3(Ptr<StateBase> s);
        static DefaultCoro func4(Ptr<StateBase> s);

        //std::vector<std::function<DefaultCoro(Ptr<StateBase>)>> vec{func1,func2,func3,func4};

    };
}
#endif /* EXAMPLE1_H */

