#ifndef DEFAULTCORO_H
#define DEFAULTCORO_H
#include"ns3/object.h"
#include"ns3/core-module.h"
#include <concepts>
#include <coroutine>
#include <exception>
#include <iostream>
#include"ns3/state-base.h"
namespace ns3{
    /* 
     * 协程的设计不需要进行约束，所以不设置Base类。
     * 时间记录和task状态记录的操作放在Awaiter里。
     * Awaiter类负责协程的开始，挂起，结束的同步操作和数据记录。
     * 
     * 作为任务，应该把对时间戳的判断逻辑放在调度器里。所以协程创建时都
     * 要阻塞在init处。
     * 但是协程不能在一开始就全部初始化，而是要在执行到它的时候才能初始化。
     * 想把协程链用sym transfer方法。直接co_await下一个。那么在初始化任务链
     * 的时候就要把第一个子任务初始化成协程并阻塞在init处。
     * */
    class DefaultCoro : public Object{
        public:    
            class promise_type {
                public: 
                    promise_type() = default;
                    DefaultCoro get_return_object() {
                        // Uses C++20 designated initializer syntax
//                        return{
//                            .m_h = std::coroutine_handle<promise_type>::from_promise(*this)
//                        };
                        return DefaultCoro(std::coroutine_handle<promise_type>::from_promise(*this));
                    }
                    std::suspend_always initial_suspend() { return {}; }
                    std::suspend_always final_suspend() noexcept { return {}; }
                    void return_value(Ptr<StateBase> s){s->Print();}
                    void unhandled_exception() {}
            };
            DefaultCoro(std::coroutine_handle<promise_type> h):m_h(h){}
            ~DefaultCoro(){}
            operator std::coroutine_handle<promise_type>() const { return m_h; }
            // A coroutine_handle<promise_type> converts to coroutine_handle<>
            operator std::coroutine_handle<>() const { return m_h; }

        private:
            std::coroutine_handle<promise_type> m_h;
    };
}
#endif /* DEFAULTCORO_H */
