/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef LINKCLIENT_H
#define LINKCLIENT_H


#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/ipv4-address.h"
#include "ns3/traced-callback.h"
#include "ns3/link-task.h"
#include "ns3/manager.h"
#include"ns3/link-task.h"
namespace ns3 {

class Socket;
class Packet;

    //主要修改：schedule和receive
    class LinkClient : public Application
    {
        public:
	        static TypeId GetTypeId(void);
	        
	        LinkClient();
	        virtual ~LinkClient();
	        
	        void SetRemote(Address ip, uint16_t port);
	
	        void SetRemote(Address addr);
	
	        void SetFill (std::string fill);

            std::string Serialization(const LinkTask& t);
            
            void ReceiveTask(const LinkTask& t);//接受一个任务之后发送出去
            void ScheduleTransmit(Time dt);

            void SetTotalTime(double time);
            void SetSendTime(double send);
            void SetTaskSample(LinkTask task);
            void SetMachineId(const std::string &id);
	
        
        protected:
            virtual void DoDispose(void);

        private:
            
	  	    virtual void StartApplication (void);
	  		virtual void StopApplication (void);
	  		
	  		  /**
	  		   * \brief Schedule the next packet transmission
	  		   * \param dt time interval between packets.
	  		   */
	  		void Send (void);
	  		
	  		  /**
	  		   * \brief Handle a packet reception.
	  		   *
	  		   * This function is called by lower layers.
	  		   *
	  		   * \param socket the socket the packet was received to.
	  		   */
	  		  void HandleRead (Ptr<Socket> socket);
	  		
//	  		  uint32_t m_count; //!< Maximum number of packets the application will send
//	  		  Time m_interval; //!< Packet inter-send time
	  		  uint32_t m_size; //!< Size of the sent packet
  		
	  		  uint32_t m_dataSize; //!< packet payload size (must be equal to m_size)
	  		  uint8_t *m_data; //!< packet payload data
	  		
//	  		  uint32_t m_sent; //!< Counter for sent packets
	  		  Ptr<Socket> m_socket; //!< Socket
	  		  Address m_peerAddress; //!< Remote peer address
	  		  uint16_t m_peerPort; //!< Remote peer port
	  		  EventId m_sendEvent; //!< Event to send the next packet
	  		
	  		  /// Callbacks for tracing the packet Tx events
	  		  TracedCallback<Ptr<const Packet> > m_txTrace;
	  		
	  		  /// Callbacks for tracing the packet Rx events
	  		  TracedCallback<Ptr<const Packet> > m_rxTrace;
              
	  		  
	  		  /// Callbacks for tracing the packet Tx events, includes source and destination addresses
	  		  TracedCallback<Ptr<const Packet>, const Address &, const Address &> m_txTraceWithAddresses;
	  		  
	  		  /// Callbacks for tracing the packet Rx events, includes source and destination addresses
	  		  TracedCallback<Ptr<const Packet>, const Address &, const Address &> m_rxTraceWithAddresses;

              double m_totalTime; //总仿真时间
              long m_send; //任务发送次数
              long m_sendCount=0;//目前已经发送的任务次数
              double m_interval=0;//任务发送的时间间隔
              LinkTask m_sampleTask;//要发送的任务样本
              bool m_head = false; //是否处于任务执行链的顶端
              std::string m_machineId;

    };
    

}


#endif /* LINKCLIENT_H */

