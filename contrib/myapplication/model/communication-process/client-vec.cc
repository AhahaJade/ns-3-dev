#include"client-vec.h"
namespace ns3{
    
    void ClientVec::AddHandle(Ptr<ClientBase> handle)
    {
        m_handle.push_back(handle);
    }

    void ClientVec::Run()
    {
        for(size_t i = 0; i != m_handle.size(); ++i)
        {
            m_handle[i]->InitialInform(m_inform);
            m_handle[i]->Handle();
            SetInform(m_handle[i]->GetInform());  
        }
    }
//    void ClientVec::SetInform(Information &globalInform)
//    {
//        globalInform.SetNode(m_inform.GetNode());
//        globalInform.SetPacket(m_inform.GetPacket());
//        globalInform.SetContent(m_inform.GetContent());
//    }

    void ClientVec::SetInform(Ptr<Node> node)
    {
        m_inform.SetNode(node);
    }

    void ClientVec::SetInform(Ptr<Packet> packet)
    {
        m_inform.SetPacket(packet);
    }

    void ClientVec::SetInform(std::string content)
    {
        m_inform.SetContent(content);
    }
    size_t ClientVec::Size(void)
    {
        return m_handle.size();
    }

    void ClientVec::SetInform(Task task)
    {
        m_inform.SetTask(task);
    }

    void ClientVec::SetInform(Information inform)
    {
        m_inform.SetNode(inform.GetNode());
        m_inform.SetPacket(inform.GetPacket());
        m_inform.SetContent(inform.GetContent());
    }
    Ptr<Packet> ClientVec::GetPacket(void)
    {
        return m_inform.GetPacket();
    }
}
