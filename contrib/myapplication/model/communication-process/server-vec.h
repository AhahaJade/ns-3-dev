#ifndef _SERVER_VEC_H
#define _SERVER_VEC_H
#include"ns3/server-base.h"
#include<vector>
namespace ns3{

    class ServerVec {
        public:
            
            ServerVec(){}
            /**
             * \Responsible for m_handle initialization
             */
            void AddHandle(Ptr<ServerBase>);

            //负责顺序调用以及参数传递。调用前把参数传进去，调用完把参数拉下来。
            void Run();

            void SetInform(Ptr<Node> node);

            void SetInform(Ptr<Packet> packet);

            void SetInform(std::string content);

            void SetInform(Information inform);

            size_t Size(void);

        private:
//            /**
//             * \get information from single HandleBase from m_handle
//             */
//            void PushInform(Information globalInform);
//
//            /**
//             * \get Information from the global Information object
//             */
//            void PullInform(Information &localInform);

            /**
             * \update information
             */

            std::vector<Ptr<ServerBase>> m_handle;
            
            /**
             * \This is global information maintained by HandleVec
             */
            Information m_inform;

    };



}





#endif
