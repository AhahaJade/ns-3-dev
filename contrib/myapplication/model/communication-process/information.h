#ifndef _INFORMATION_H
#define _INFORMATION_H
#include"ns3/ptr.h"
#include"ns3/node.h"
#include<string>
#include"ns3/packet.h"
#include"ns3/task.h"
namespace ns3{
    class Information : public Object{
        public:
            
            static TypeId GetTypeId(void);
            Information() = default;
            Information(Ptr<Node> node,Ptr<Packet> packet):m_node(node),m_packet(packet){}
            Ptr<Node> GetNode();
            Ptr<Packet> GetPacket();
            std::string GetContent();
            Task GetTask();
            void SetNode(Ptr<Node> node);
            void SetPacket(Ptr<Packet> packet);
            void SetContent(const std::string& content);
            void SetTask(const Task& task);

        private:

            Task m_task;
            Ptr<Node> m_node;
            Ptr<Packet> m_packet;
            std::string m_content = "";

    };
}





#endif
