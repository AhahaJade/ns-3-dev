#include"ns3/link-service-c.h"
namespace ns3{
    
    NS_LOG_COMPONENT_DEFINE("LinkServiceC");
    NS_OBJECT_ENSURE_REGISTERED(LinkServiceC);
    
    TypeId  LinkServiceC::GetTypeId(void){
        static TypeId tid = TypeId("ns3::LinkServiceC")
            .SetParent<Object>()
            .SetGroupName("Applications");
            
        return tid;
    }
    
    void LinkServiceC::AddHandle(Ptr<ClientBase> handle){
        m_clientVec.AddHandle(handle);
    }
    void LinkServiceC::SetInform(Task task){
        m_clientVec.SetInform(task);
    }

    void LinkServiceC::SetInform(Information inform){
        m_clientVec.SetInform(inform);
    }


    void LinkServiceC::SendHandle(Ptr<Socket> socket,Task t){
        Task task = t;    
        m_clientVec.SetInform(task);
        m_clientVec.Run();
        //顺序执行m_clientVec中的操作
        Ptr<Packet> p = m_clientVec.GetPacket();

        //Do the sending stuf
        Address localAddress;
        socket->GetSockName (localAddress);
        socket->Send (p);

//        if (Ipv4Address::IsMatchingType (m_peerAddress))
//        {
//            NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) << " client sent " << m_size << " bytes to " <<
//                    Ipv4Address::ConvertFrom (m_peerAddress) << " port " << m_peerPort);
//        }
//        else if (Ipv6Address::IsMatchingType (m_peerAddress))
//        {
//            NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) << " client sent " << m_size << " bytes to " <<
//                    Ipv6Address::ConvertFrom (m_peerAddress) << " port " << m_peerPort);
//        }
//        else if (InetSocketAddress::IsMatchingType (m_peerAddress))
//        {
//            NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) << " client sent " << m_size << " bytes to " <<
//                    InetSocketAddress::ConvertFrom (m_peerAddress).GetIpv4 () << " port " << InetSocketAddress::ConvertFrom (m_peerAddress).GetPort ());
//        }
//        else if (Inet6SocketAddress::IsMatchingType (m_peerAddress))
//        {
//            NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) << " client sent " << m_size << " bytes to " <<
//                    Inet6SocketAddress::ConvertFrom (m_peerAddress).GetIpv6 () << " port " << Inet6SocketAddress::ConvertFrom (m_peerAddress).GetPort ());
//        }
    }



    
    

}
