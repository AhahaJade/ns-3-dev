
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv6-address.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/trace-source-accessor.h"
#include "client.h"
#include </usr/local/include/boost/serialization/serialization.hpp>
#include </usr/local/include/boost/archive/text_iarchive.hpp>
#include </usr/local/include/boost/archive/text_oarchive.hpp>
#include<cstring>
namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Client");

NS_OBJECT_ENSURE_REGISTERED (Client);

TypeId
Client::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::Client")
    .SetParent<Application> ()
    .SetGroupName("MyApplications")
    .AddConstructor<Client> ()
    .AddAttribute ("RemoteAddress", 
                   "The destination Address of the outbound packets",
                   AddressValue (),
                   MakeAddressAccessor (&Client::m_peerAddress),
                   MakeAddressChecker ())
    .AddAttribute ("RemotePort", 
                   "The destination port of the outbound packets",
                   UintegerValue (0),
                   MakeUintegerAccessor (&Client::m_peerPort),
                   MakeUintegerChecker<uint16_t> ())
  ;
  return tid;
}
Client::Client ()
{
  NS_LOG_FUNCTION (this);
  m_socket = 0;
  m_sendEvent = EventId ();
  m_factory = CreateObject<ClientFactory>();
  m_service = CreateObject<LinkServiceC>();
  m_service->AddHandle(m_factory->Create("Serialize"));
  m_service->AddHandle(m_factory->Create("Packet"));
}

Client::~Client()
{
  NS_LOG_FUNCTION (this);
  m_socket = 0;

}

void 
Client::SetRemote (Address ip, uint16_t port)
{
  NS_LOG_FUNCTION (this << ip << port);
  m_peerAddress = ip;
  m_peerPort = port;
}
void 
Client::SetRemote (Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  m_peerAddress = addr;
}
void
Client::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  Application::DoDispose ();
}
void 
Client::StartApplication (void)
{
  NS_LOG_FUNCTION (this);

  if (m_socket == 0)
    {
      TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
      m_socket = Socket::CreateSocket (GetNode (), tid);
      if (Ipv4Address::IsMatchingType(m_peerAddress) == true)
        {
          if (m_socket->Bind () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (InetSocketAddress (Ipv4Address::ConvertFrom(m_peerAddress), m_peerPort));
        }
      else if (Ipv6Address::IsMatchingType(m_peerAddress) == true)
        {
          if (m_socket->Bind6 () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (Inet6SocketAddress (Ipv6Address::ConvertFrom(m_peerAddress), m_peerPort));
        }
      else if (InetSocketAddress::IsMatchingType (m_peerAddress) == true)
        {
          if (m_socket->Bind () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (m_peerAddress);
        }
      else if (Inet6SocketAddress::IsMatchingType (m_peerAddress) == true)
        {
          if (m_socket->Bind6 () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (m_peerAddress);
        }
      else
        {
          NS_ASSERT_MSG (false, "Incompatible address type: " << m_peerAddress);
        }
    }

  m_socket->SetRecvCallback (MakeCallback (&Client::HandleRead, this));
  m_socket->SetAllowBroadcast (true);
  if(!m_readyToSend.empty())
  {
      ScheduleTransmit(m_readyToSend.begin()->second-Now());
  }
}
void 
Client::StopApplication ()
{
  NS_LOG_FUNCTION (this);

  if (m_socket != 0) 
    {
      m_socket->Close ();
      m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
      m_socket = 0;
    }

  Simulator::Cancel (m_sendEvent);
}
//uint32_t 
//Client::GetDataSize (void) const
//{
//  NS_LOG_FUNCTION (this);
//  return m_size;
//}

void Client::ReceiveTask(const Task& t,Time dt)
{
    //std::cout<<"Client::ReceiveTask"<<std::endl;
    auto it = find_if(m_readyToSend.begin(),m_readyToSend.end(),[dt](std::pair<Task,Time> temp){return dt<temp.second; });
    auto temp = std::make_pair(t,dt);
    
    m_readyToSend.insert(it,temp);

}
void Client::ScheduleTransmit(Time dt)
{
    Simulator::Schedule(dt,&Client::Send,this);
}

void 
Client::Send (void)
{
  NS_LOG_FUNCTION (this);

  NS_ASSERT (m_sendEvent.IsExpired ());

  auto pair = m_readyToSend.front();
  m_readyToSend.pop_front();
  Task task = pair.first;    
  m_service->SendHandle(m_socket,task);

  
  if(!m_readyToSend.empty())
  {
      ScheduleTransmit(m_readyToSend.begin()->second-Now());
  }

}
void
Client::HandleRead (Ptr<Socket> socket)
{
    ;
//  NS_LOG_FUNCTION (this << socket);
//  Ptr<Packet> packet;
//  Address from;
//  Address localAddress;
//  while ((packet = socket->RecvFrom (from)))
//    {
//      socket->GetSockName (localAddress);
//      m_rxTrace (packet);
//      m_rxTraceWithAddresses (packet, from, localAddress);
//      uint32_t size = packet->GetSize();
//      uint8_t *data = new uint8_t[size];
//      packet->CopyData(data,size);
//      std::string content(data,data+size);
//      if (InetSocketAddress::IsMatchingType (from))
//        {
//          NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) << " client received       " << content << "        from " <<
//                       InetSocketAddress::ConvertFrom (from).GetIpv4 () << " port " <<
//                       InetSocketAddress::ConvertFrom (from).GetPort ());
//        }
//      else if (Inet6SocketAddress::IsMatchingType (from))
//        {
//          NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) << " client received       "  << content << "       from " <<
//                       Inet6SocketAddress::ConvertFrom (from).GetIpv6 () << " port " <<
//                       Inet6SocketAddress::ConvertFrom (from).GetPort ());
//        }
////      socket->GetSockName (localAddress);
////      m_rxTrace (packet);
////      m_rxTraceWithAddresses (packet, from, localAddress);
////      uint32_t size = packet->GetSize();
////      uint8_t *data = new uint8_t[size];
////      packet->CopyData(data,size);
////      std::string task_string(data,data+size);
////      auto taskPair = Deserialization(task_string);
////      if(taskPair.second==true)
////      {
////          Ptr<Node> node = GetNode();
////          Ptr<Manager> manager = node->GetObject<Manager>();
////          manager->ReceiveTask(taskPair.first);
////      }
//
//
    
}

}

