#ifndef _PACKET_CREARTION_HANDLE_H
#define _PACKET_CREARTION_HANDLE_H
#include"client-base.h"
namespace ns3{
    class PacketCreationHandle :public ClientBase{

        public:
            static TypeId GetTypeId(void);

            virtual void Handle() override;

        private:

            void SetFill(std::string fill);

            uint32_t m_dataSize;

            uint8_t *m_data;

    };
}
#endif
