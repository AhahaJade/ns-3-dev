#include"serialize-handle.h"
namespace ns3{
    
    NS_LOG_COMPONENT_DEFINE("SerializeHandle");
    NS_OBJECT_ENSURE_REGISTERED(SerializeHandle);

    TypeId SerializeHandle::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::SerializeHandle")
            .SetParent<ClientBase>()
            .SetGroupName("Applications");
        return tid;
            
    }

    void SerializeHandle::Handle()
    {
        Task task = m_inform.GetTask();
        std::string content = Serialization(task);
        m_inform.SetContent(content);
    }


    std::string SerializeHandle::Serialization(const Task& t)
    {
	    std::ostringstream oss;
	    boost::archive::text_oarchive oa(oss);
	    oa<<t;
	    return oss.str();
    }

}
