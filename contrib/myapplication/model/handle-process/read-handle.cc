#include"read-handle.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("ReadHandle");
    NS_OBJECT_ENSURE_REGISTERED(ReadHandle);

    TypeId ReadHandle::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::ReadHandle")
            .SetParent<ServerBase>()
            .SetGroupName("Applications")
            .AddConstructor<ReadHandle>();
        return tid;
    }

    void ReadHandle::Handle()
    {
        Ptr<Packet> packet = m_inform.GetPacket();
        uint32_t size = packet->GetSize();
        uint8_t *data = new uint8_t[size];
        packet->CopyData(data,size);
        std::string task_string(data,data+size);
        m_inform.SetContent(task_string);
    }

}
