#include"packet-creation-handle.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("PacketCreationHandle");
    NS_OBJECT_ENSURE_REGISTERED(PacketCreationHandle);

    TypeId PacketCreationHandle::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::PacketCreationHandle")
            .SetParent<ClientBase>()
            .SetGroupName("Applications")
            .AddConstructor<PacketCreationHandle>();
        return tid;
    }

    void PacketCreationHandle::Handle()
    {
        std::string fill = m_inform.GetContent();
        SetFill(fill);
        
        Ptr<Packet> p;
        if (m_dataSize)
        {
            //
            // If m_dataSize is non-zero, we have a data buffer of the same size that we
            // are expected to copy and send.  This state of affairs is created if one of
            // the Fill functions is called.  In this case, m_size must have been set
            // to agree with m_dataSize
            //
            p = Create<Packet> (m_data, m_dataSize);
        }
        else
        { 
            std::cerr<<"Application can't send an empty packet ";
        }

        m_inform.SetPacket(p);

    }

    void PacketCreationHandle::SetFill(std::string fill)
    {
        NS_LOG_FUNCTION (this << fill);

        uint32_t dataSize = fill.size () + 1;

        if (dataSize != m_dataSize)
        {
            delete [] m_data;
            m_data = new uint8_t [dataSize];
            m_dataSize = dataSize;
        }

        memcpy (m_data, fill.c_str (), dataSize);

        m_dataSize = dataSize;
        
    }

}
