#include"deserialize-handle.h"
namespace ns3{
    
    NS_LOG_COMPONENT_DEFINE("DeserializeHandle");
    NS_OBJECT_ENSURE_REGISTERED(DeserializeHandle);

    TypeId DeserializeHandle::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::DeserializeHandle")
            .SetParent<ServerBase>()
            .SetGroupName("Applications");
        return tid;
            
    }

    void DeserializeHandle::Handle()
    {
        std::string task_string = m_inform.GetContent();
        auto taskPair = Deserialization(task_string);
        if(taskPair.second==true)
        {
            Ptr<Node> node = m_inform.GetNode();
            Ptr<Manager> manager = node->GetObject<Manager>();
            auto task = taskPair.first;
            task->SetGenerateTime(-1);
            task->SetEndTime(-1);
            manager->ReceiveTask(taskPair.first);
        }
    }


    std::pair<Ptr<Task>,bool> DeserializeHandle::Deserialization(const std::string& t)
    {
        const std::string prefix = "22 serialization::archive";
        if(t.find(prefix)==std::string::npos)
        {
            Task temp;//默认初始化表示反序列化失败
            Ptr<Task> newtask = CreateObject<Task>(temp);
            return std::make_pair(newtask,false);
        }
        std::istringstream iss(t);
        boost::archive::text_iarchive ia(iss);
        Task temp;
        ia>>temp;
        Ptr<Task> newtask = CreateObject<Task>(temp);
        newtask->SetGenerateTime(-1);
        newtask->SetEndTime(-1);
        //std::cout<<newtask->GetGenerateTime()<<'\t'<<newtask->GetEndTime()<<std::endl;
        return std::make_pair(newtask,true);
    }

}
