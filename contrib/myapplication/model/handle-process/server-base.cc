#include"ns3/server-base.h"
namespace ns3{
    
    NS_LOG_COMPONENT_DEFINE("ServerBase");
    NS_OBJECT_ENSURE_REGISTERED(ServerBase);
    TypeId ServerBase::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::ServerBase")
            .SetParent<HandleBase>()
            .SetGroupName("Applications");
        return tid;
            
    }

    void ServerBase::InitialInform(Information inform){
        m_inform.SetNode(inform.GetNode());
        m_inform.SetPacket(inform.GetPacket());
        m_inform.SetContent(inform.GetContent());
    }
            
    Information ServerBase::GetInform(void)
    {
        return m_inform;
    }


}
