
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv6-address.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/trace-source-accessor.h"
#include "link-client.h"
#include </usr/local/include/boost/serialization/serialization.hpp>
#include </usr/local/include/boost/archive/text_iarchive.hpp>
#include </usr/local/include/boost/archive/text_oarchive.hpp>
#include<cstring>
namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("LinkClient");

NS_OBJECT_ENSURE_REGISTERED (LinkClient);

TypeId
LinkClient::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::LinkClient")
    .SetParent<Application> ()
    .SetGroupName("MyApplications")
    .AddConstructor<LinkClient> ()
    .AddAttribute ("RemoteAddress", 
                   "The destination Address of the outbound packets",
                   AddressValue (),
                   MakeAddressAccessor (&LinkClient::m_peerAddress),
                   MakeAddressChecker ())
    .AddAttribute ("RemotePort", 
                   "The destination port of the outbound packets",
                   UintegerValue (0),
                   MakeUintegerAccessor (&LinkClient::m_peerPort),
                   MakeUintegerChecker<uint16_t> ())
    //最好是能加一个对task的初始化。
  ;
  return tid;
}
LinkClient::LinkClient ()
{
  NS_LOG_FUNCTION (this);
  m_socket = 0;
  m_sendEvent = EventId ();
  m_data = 0;
  m_dataSize = 0;
}

LinkClient::~LinkClient()
{
  NS_LOG_FUNCTION (this);
  m_socket = 0;

  delete [] m_data;
  m_data = 0;
  m_dataSize = 0;
}

void 
LinkClient::SetRemote (Address ip, uint16_t port)
{
  NS_LOG_FUNCTION (this << ip << port);
  m_peerAddress = ip;
  m_peerPort = port;
}
void 
LinkClient::SetRemote (Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  m_peerAddress = addr;
}
void
LinkClient::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  Application::DoDispose ();
}
void 
LinkClient::StartApplication (void)
{
  NS_LOG_FUNCTION (this);

  if (m_socket == 0)
    {
      TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
      m_socket = Socket::CreateSocket (GetNode (), tid);
      if (Ipv4Address::IsMatchingType(m_peerAddress) == true)
        {
          if (m_socket->Bind () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (InetSocketAddress (Ipv4Address::ConvertFrom(m_peerAddress), m_peerPort));
        }
      else if (Ipv6Address::IsMatchingType(m_peerAddress) == true)
        {
          if (m_socket->Bind6 () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (Inet6SocketAddress (Ipv6Address::ConvertFrom(m_peerAddress), m_peerPort));
        }
      else if (InetSocketAddress::IsMatchingType (m_peerAddress) == true)
        {
          if (m_socket->Bind () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (m_peerAddress);
        }
      else if (Inet6SocketAddress::IsMatchingType (m_peerAddress) == true)
        {
          if (m_socket->Bind6 () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (m_peerAddress);
        }
      else
        {
          NS_ASSERT_MSG (false, "Incompatible address type: " << m_peerAddress);
        }
    }

  m_socket->SetRecvCallback (MakeCallback (&LinkClient::HandleRead, this));
  m_socket->SetAllowBroadcast (true);
}
void 
LinkClient::StopApplication ()
{
  NS_LOG_FUNCTION (this);

  if (m_socket != 0) 
    {
      m_socket->Close ();
      m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
      m_socket = 0;
    }

  Simulator::Cancel (m_sendEvent);
}
void 
LinkClient::SetFill (std::string fill)
{
  NS_LOG_FUNCTION (this << fill);

  uint32_t dataSize = fill.size () + 1;

  if (dataSize != m_dataSize)
    {
      delete [] m_data;
      m_data = new uint8_t [dataSize];
      m_dataSize = dataSize;
    }

  memcpy (m_data, fill.c_str (), dataSize);

  m_size = dataSize;
}
std::string LinkClient::Serialization(const LinkTask& t)
{
    std::ostringstream oss;
    boost::archive::text_oarchive oa(oss);
    oa<<t;
    return oss.str();
}
//std::pair<Ptr<Task>,bool> LinkClient::Deserialization(const std::string& t)
//{
//    const std::string prefix = "22 serialization::archive";
//    if(t.find(prefix)==std::string::npos)
//    {
//        Task temp;//默认初始化表示反序列化失败
//        Ptr<Task> newtask = CreateObject<Task>(temp);
//        return std::make_pair(newtask,false);
//    }
//    std::istringstream iss(t);
//    boost::archive::text_iarchive ia(iss);
//    Task temp;
//    ia>>temp;
//    Ptr<Task> newtask = CreateObject<Task>(temp);
//    return std::make_pair(newtask,true);
//}

void LinkClient::ReceiveTask(const LinkTask& t)
{
    m_sampleTask = t;
    m_sampleTask.sendTest = Now().GetSeconds();
    //这一步可能会报错，在时间上，一Simulator::start就会调用StartApplication
    //所以这一步以及发送时间和发送次数的初始化要先于start.
    ScheduleTransmit(Seconds(0.));

    //std::cout<<"LinkClient::ReceiveTask"<<std::endl;
//    m_task = t;    
//    std::string task_string = Serialization(m_task);
//    SetFill(task_string);
//    Send();

}
void LinkClient::ScheduleTransmit(Time dt)
{
    //std::cout<<1<<std::endl;
    m_sendEvent = Simulator::Schedule(dt,&LinkClient::Send,this);
}
void LinkClient::SetTotalTime(double time)
{
    m_totalTime = time;
    m_head = true;
    if(m_send)
    {
        m_interval = m_totalTime/m_send;
    }
}
void LinkClient::SetSendTime(double send)
{
    m_send = send;
    m_head = true;
    if(m_totalTime)
    {
        m_interval = m_totalTime/m_send;
    }
}
void LinkClient::SetTaskSample(LinkTask task)
{
    m_sampleTask = task;
    //具体的task内部数据初始化就在main函数里面执行吧
}
void LinkClient::SetMachineId(const std::string &id)
{
    m_machineId = id;
}

void 
LinkClient::Send (void)
{
  NS_LOG_FUNCTION (this);

  //NS_ASSERT (m_sendEvent.IsExpired ());

  std::string task_string = Serialization(m_sampleTask);
  SetFill(task_string);

  Ptr<Packet> p;
  if (m_dataSize)
    {
      //
      // If m_dataSize is non-zero, we have a data buffer of the same size that we
      // are expected to copy and send.  This state of affairs is created if one of
      // the Fill functions is called.  In this case, m_size must have been set
      // to agree with m_dataSize
      //
      NS_ASSERT_MSG (m_dataSize == m_size, "UdpEchoLinkClient::Send(): m_size and m_dataSize inconsistent");
      NS_ASSERT_MSG (m_data, "UdpEchoLinkClient::Send(): m_dataSize but no m_data");
      p = Create<Packet> (m_data, m_dataSize);
    }
  else
    { 
      std::cerr<<"Application can't send an empty packet ";
    }
  Address localAddress;
  m_socket->GetSockName (localAddress);
  // call to the trace sinks before the packet is actually sent,
  // so that tags added to the packet can be sent as well
  m_txTrace (p);
  if (Ipv4Address::IsMatchingType (m_peerAddress))
    {
      m_txTraceWithAddresses (p, localAddress, InetSocketAddress (Ipv4Address::ConvertFrom (m_peerAddress), m_peerPort));
    }
  else if (Ipv6Address::IsMatchingType (m_peerAddress))
    {
      m_txTraceWithAddresses (p, localAddress, Inet6SocketAddress (Ipv6Address::ConvertFrom (m_peerAddress), m_peerPort));
    }
//      std::cout<<"next task"<<std::endl;
  m_socket->Send (p);
  ++m_sendCount;

  if (Ipv4Address::IsMatchingType (m_peerAddress))
    {
      NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) <<"  machien: "<<m_machineId <<"  sent " << m_size << " bytes to " <<
                   Ipv4Address::ConvertFrom (m_peerAddress) << " port " << m_peerPort);
    }
  else if (Ipv6Address::IsMatchingType (m_peerAddress))
    {NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) <<"  machien: "<<m_machineId <<"  sent " << m_size << " bytes to " <<
                   Ipv6Address::ConvertFrom (m_peerAddress) << " port " << m_peerPort);
    }
  else if (InetSocketAddress::IsMatchingType (m_peerAddress))
    {NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) <<"  machien: "<<m_machineId <<"  sent " << m_size << " bytes to " <<
                   InetSocketAddress::ConvertFrom (m_peerAddress).GetIpv4 () << " port " << InetSocketAddress::ConvertFrom (m_peerAddress).GetPort ());
    }
  else if (Inet6SocketAddress::IsMatchingType (m_peerAddress))
    {NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) <<"  machien: "<<m_machineId <<"  sent " << m_size << " bytes to " <<
                   Inet6SocketAddress::ConvertFrom (m_peerAddress).GetIpv6 () << " port " << Inet6SocketAddress::ConvertFrom (m_peerAddress).GetPort ());
    }
//  if(m_head&&(m_sendCount<m_send))
//  {
//      ScheduleTransmit(Seconds(m_interval));
//  }

}
void
LinkClient::HandleRead (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  Ptr<Packet> packet;
  Address from;
  Address localAddress;
  while ((packet = socket->RecvFrom (from)))
    {
      socket->GetSockName (localAddress);
      m_rxTrace (packet);
      m_rxTraceWithAddresses (packet, from, localAddress);
      uint32_t size = packet->GetSize();
      uint8_t *data = new uint8_t[size];
      packet->CopyData(data,size);
      std::string content(data,data+size);
      if (InetSocketAddress::IsMatchingType (from))
        {
          NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) <<"  machien: "<<m_machineId <<"  received       " << content << "        from " <<
                       InetSocketAddress::ConvertFrom (from).GetIpv4 () << " port " <<
                       InetSocketAddress::ConvertFrom (from).GetPort ());
        }
      else if (Inet6SocketAddress::IsMatchingType (from))
        {
          NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) <<"  machien: "<<m_machineId <<"  received       " << content << "        from " <<
                       Inet6SocketAddress::ConvertFrom (from).GetIpv6 () << " port " <<
                       Inet6SocketAddress::ConvertFrom (from).GetPort ());
        }
//      socket->GetSockName (localAddress);
//      m_rxTrace (packet);
//      m_rxTraceWithAddresses (packet, from, localAddress);
//      uint32_t size = packet->GetSize();
//      uint8_t *data = new uint8_t[size];
//      packet->CopyData(data,size);
//      std::string task_string(data,data+size);
//      auto taskPair = Deserialization(task_string);
//      if(taskPair.second==true)
//      {
//          Ptr<Node> node = GetNode();
//          Ptr<Manager> manager = node->GetObject<Manager>();
//          manager->ReceiveTask(taskPair.first);
//      }


    }
}

}

