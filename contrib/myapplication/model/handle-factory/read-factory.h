#ifndef _READ_FACTORY_H
#define _READ_FACTORY_H
#include"ns3/read-handle.h"
#include"ns3/server-factory-base.h"
namespace ns3{
    struct ReadFactory:virtual public ServerFactoryBase{
        static TypeId GetTypeId(void); 

        Ptr<ServerBase> Create() override{
            return CreateObject<ReadHandle>();
        }
    };
}
#endif
