#ifndef _MMCLIENT_FACTORY_H
#define _MMCLIENT_FACTORY_H
#include"ns3/packet-creation-factory.h"
#include"ns3/serialize-factory.h"
#include<map>
#include<string>
namespace ns3{
    class ClientFactory:public Object{

        public:
            static TypeId GetTypeId(void); 
            ClientFactory();
            
            Ptr<ClientBase> Create(const std::string& name);

        private:
            std::map<std::string,Ptr<ClientFactoryBase>> m_clientFactories;
    };
}
#endif
