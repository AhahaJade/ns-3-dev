#include"ns3/deserialize-factory.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("DeserializeFactory");
    NS_OBJECT_ENSURE_REGISTERED(DeserializeFactory);

    TypeId DeserializeFactory::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::DeserializeFactory")
            .SetParent<ServerFactoryBase>()
            .SetGroupName("Applications")
            .AddConstructor<DeserializeFactory>();
        return tid;
    }
    
}
