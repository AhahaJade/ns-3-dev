#include"ns3/packet-creation-factory.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("PacketCreationFactory");
    NS_OBJECT_ENSURE_REGISTERED(PacketCreationFactory);

    TypeId PacketCreationFactory::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::PacketCreationFactory")
            .SetParent<ClientFactoryBase>()
            .SetGroupName("Applications")
            .AddConstructor<PacketCreationFactory>();
        return tid;
    }
    
}
