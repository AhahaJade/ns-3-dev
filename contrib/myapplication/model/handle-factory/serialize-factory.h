#ifndef _SERIALIZE_FACTORY_H
#define _SERIALIZE_FACTORY_H
#include"ns3/serialize-handle.h"
#include"ns3/client-factory-base.h"
namespace ns3{
    class SerializeHandle;
    struct SerializeFactory:virtual public ClientFactoryBase{
        static TypeId GetTypeId(void); 

        Ptr<ClientBase> Create() override{
            return CreateObject<SerializeHandle>();
        }
    };
}
#endif
