#include"ns3/server-factory-base.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("ServerFactoryBase");
    NS_OBJECT_ENSURE_REGISTERED(ServerFactoryBase);

    TypeId ServerFactoryBase::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::ServerFactoryBase")
            .SetParent<Object>()
            .SetGroupName("Applications");
        return tid;
    }
    
}
