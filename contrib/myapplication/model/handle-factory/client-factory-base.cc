#include"ns3/client-factory-base.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("ClientFactoryBase");
    NS_OBJECT_ENSURE_REGISTERED(ClientFactoryBase);

    TypeId ClientFactoryBase::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::ClientFactoryBase")
            .SetParent<Object>()
            .SetGroupName("Applications");
        return tid;
    }
    
}
