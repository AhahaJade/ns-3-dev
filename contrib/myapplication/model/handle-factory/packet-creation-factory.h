#ifndef _PACKET_CREATION_FACTORY_H
#define _PACKET_CREATION_FACTORY_H
#include"ns3/packet-creation-handle.h"
#include"ns3/client-factory-base.h"
namespace ns3{
    struct PacketCreationFactory:virtual public ClientFactoryBase{
        static TypeId GetTypeId(void); 

        Ptr<ClientBase> Create() override{
            return CreateObject<PacketCreationHandle>();
        }
    };
}
#endif
