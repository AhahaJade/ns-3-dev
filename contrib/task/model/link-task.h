/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef LINK_TASK_H
#define LINK_TASK_H
#include"ns3/core-module.h"
#include"ns3/object.h"
#include"ns3/ptr.h"
#include"ns3/type-id.h"
#include"ns3/node.h"
#include"ns3/traced-value.h"
#include"ns3/orchestratorbase.h"
#include <boost/serialization/serialization.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <list>
#include <boost/serialization/list.hpp>
/*应用场景，单个task需要经过多个设备的执行才能完成
 * */
namespace ns3 {
    class OrchestratorBase;
    class LinkTask : public Task{
        friend class boost::serialization::access;
        public:
            LinkTask(double requestcpu,double requestmem,std::initializer_list<std::string> machineid):Task(requestcpu,requestmem){
                InitialMachineList(machineid);
                SetWaitTime(Seconds(20));
            }
            LinkTask() =default;
            virtual~LinkTask(){};
            void InitialMachineList(std::initializer_list<std::string> machineid);
            template<class Ar> void serialize(Ar &ar,const unsigned int version)
            {
                ar.template register_type<Task>();
                ar & boost::serialization::base_object<Task>(*this);
                ar & m_machine_list;
                ar & m_time;
                ar & sendTest;
                ar & receiveTest;
            }
            void SetTime(double t)
            {
                m_time =t;
            }
            double GetTime()
            {
                return m_time;
            }
            std::list<std::string> GetMachineList();
            std::string PopMachine();
            std::string GetMachine();
            //以下两个用于测试
            double sendTest;
            double receiveTest;
            

        private:
            std::list<std::string> m_machine_list;//代表任务需按顺序经由这些设备处理  
            double m_time;
    };
}
#endif /* LINK_TASK_H */

