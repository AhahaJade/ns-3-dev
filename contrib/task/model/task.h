/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef TASK_H
#define TASK_H
#include"ns3/core-module.h"
#include"ns3/object.h"
#include"ns3/ptr.h"
#include"ns3/type-id.h"
#include"ns3/node.h"
#include"ns3/traced-value.h"
#include"ns3/orchestratorbase.h"
#include <boost/serialization/serialization.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
namespace ns3 {
    class OrchestratorBase;
    class Task:public Object{
        friend class OrchestratorBase;
        friend class boost::serialization::access;
        public:
            Task(){m_requestCpu=0;m_requestMemory=0;      }
            virtual ~Task(){}
            static TypeId GetTypeId(void);
//            Task(std::string taskid,double requestcpu,double requestmem):m_taskId(taskid),m_requestCpu(requestcpu),m_requestMemory(requestmem
//){};
//            Task(double requestcpu,double requestmem):m_requestCpu(requestcpu),m_requestMemory(requestmem){};
//            Task(std::string id,double requestcpu,double requestmem,std::size_t prior,int cate);
//            Task(std::string taskid,double requestcpu,double requestmem,std::size_t prior,std::string machineid):m_taskId(taskid),m_requestCpu(requestcpu),m_requestMemory(requestmem),m_priority(prior),m_originMachineId(machineid){};
            std::string GetTId(void) const;
            double GetRCpu(void) const;
            double GetRMemory(void) const;
            void ChangePriority(std::size_t prior);
            std::size_t GetPriority(void) const;
            std::string GetOriginMId(void) const;
            void ChangeOriginMId(std::string originMId);
            void ChangeNowMId(std::string id);
            std::string GetNowMId(void) const;
            void ChangeDestinationMId(std::string id);
            std::string GetDestinationMId(void) const;
  		    template<class Ar> void serialize(Ar& ar, const unsigned int version)
  		    {
  		      ar & m_taskId;
              ar & m_requestCpu;
              ar & m_requestMemory;
              ar & m_priority;
              ar & m_destinationMachineId;
              ar & m_generateTimeD;
              ar & m_endTimeD;
              //ar & m_copyDestinationMId;
              //ar & m_originMachineId;
              //ar & m_nowMachineId;
              //ar & m_categoryInt;
              //ar & m_categoryString;
  		    }
/*-------------------status public function--------------------------*/

            void Unsubmitted(void);//这一状态包括任务刚刚创建以及当前machine刚刚接收此任务
            void Pending(void);
            void Running(void);
            void Dead(void);
            std::string GetCategoryString(void) const;
            int GetCategoryInt(void) const;
            void SetGenerateTime(double dt);
            void SetGenerateTime(void) ;
            Time GetGenerateTime(void) const;
            void SetWaitTime(Time dt);
            Time GetWaitTime(void) const;
            void SetEndTime(double dt);
            Time GetEndTime(void) const;

       
            Task(std::string taskid,double requestcpu,double requestmem,std::size_t prior,std::string destination=""):m_taskId(taskid),m_requestCpu(requestcpu),m_requestMemory(requestmem),m_priority(prior),m_destinationMachineId(destination),m_copyDestinationMId(destination){}
            Task(double requestcpu,double requestmem):m_requestCpu(requestcpu),m_requestMemory(requestmem){}
            Time m_during;//表示任务的执行时间
            double m_generateTimeD=-1;
        private:
            std::string m_taskId="";              //任务的id
            double m_requestCpu;
            double m_requestMemory;
            std::size_t m_priority=0;           //任务的优先级
            std::string m_originMachineId="";    //生成任务的机器id
            std::string m_destinationMachineId="";//空字符串可代表本机id
            std::string m_copyDestinationMId;//TracedValue在序列话时并不兼容，所以采取这种方法进行备份
            TracedValue<std::string> m_nowMachineId="";  //表明任务当前所在machine的id
/*----------------------status information--------------------------*/
            void  ChangeStateInfo(int cate); 
            int m_categoryInt=2;
            TracedValue<std::string> m_categoryString="Pending"; //用于trace对外输出，string会比int更加直观
            Time m_generateTime = Seconds(0);//表示任务生成的时间点
            Time m_waitTime=Seconds(10);//表示任务的最长等待时间
            Time m_endTime=Seconds(100000);//表示任务结束的时间
//            double m_generateTimeD=-1;
            double m_endTimeD=0;
            //因为boost无法序列化Time，所以创建两个double变量暂存Time值，用于序列化构建Packet。


};
}
#endif /* TASK_H */

